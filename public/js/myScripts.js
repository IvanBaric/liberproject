/**
 * Created by Ivan on 8.5.2016..
 */
$(document).ready( function() {
    $( ".delete" ).on( "click", function(event) {
        event.preventDefault();
        var url = $( this ).attr("href");

        sweetAlertConfirm(url);
    });
});

$(document).ready( function() {
    $( ".orphanError" ).on( "click", function(event) {
        event.preventDefault();
        var msg = $( this ).attr("data-message");

        orphanError(msg);
    });
});

$(document).ready( function() {
    $( ".confirmDelete" ).on( "click", function(event) {
        event.preventDefault();
        var url = $( this ).attr("href");

        confirmDelete(url);
    });
});

$(document).ready( function(){
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").fadeOut(500);
    });
});


$(document).ready(function() {
    $(".ajaxSort").on( "click", function(event) {
        event.preventDefault();



        //var clicked = $(this).attr("data-clicked","true");

        var page = $(this).attr( "data-page" );
        var order_by = $(this).attr( "data-order_by" );
        var order = $(this).attr( "data-order" );
        var search = $(this).attr( "data-search" );
        var route = $(this).attr( "data-route" );
        var category = $(this).attr( "data-category" );
        var tag = $(this).attr( "data-tag" );

//alert(category);


        $.ajax({
            cache: false,
            type: 'POST',
            url: route,
            data: {page:page, order_by:order_by, order:order, search:search, category:category, tag:tag},

            beforeSend: function(){
                $("#results").fadeTo( 'slow', 0.2);
            },

            success: function(data) {
                $("#results").fadeTo( 'fast', 1);
                $("#results").html( data );
            },
        });
    });

});



function orphanError(msg){
    swal(
        'Sigurnosna provjera',
        msg,
        'error'
    )
}




function sweetAlertConfirm(url){
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function(isConfirm) {
        if (isConfirm) {
            swal("Update confirmed!", "Record is going to be updated.", "success");
            window.location.replace(url);

        } else {
            swal("Cancelled", "Record is not going to be updated.)", "error");
        }
    })
}



function confirmDelete(url){
    swal({
        title: 'Upozorenje',
        text: "Želite li nastaviti s brisanjem djela iz knjižničnog kataloga?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '<i class="fa fa-check-square-o" aria-hidden="true"></i>  Nastavi s brisanjem',
        cancelButtonText: '<i class="fa fa-times" aria-hidden="true"></i> Odustani'
    }).then(function(isConfirm) {
        if (isConfirm) {
            window.location.replace(url);
        }
    })

}


$(document).ready( function() {
    function add_fieldset() {
        var currentCount = $('form > fieldset > fieldset').length;
        var template = $('form > fieldset > span').data('template');
        template = template.replace(/__index__/g, currentCount);

        $('form > fieldset').append(template);

        return false;
    }

    function remove_fieldset() {
        //write your logic to remove the last, or the current element, for isntance:

        $('form > fieldset > fieldset').last().remove();
        return false;
    }

});