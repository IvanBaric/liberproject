

<!-- page title -->
<div class="page-title">
    <h1>Heading</h1>
    <p>Subheading</p>

    <!-- breadcrumb -->
    <ul class="breadcrumb">
        <li><a href="#">Link</a></li>
        <li>Static</li>
    </ul>
    <!-- ./breadcrumb -->
</div>
<!-- ./page title -->

<!-- content wrapper -->
<div class="wrapper"></div> <!-- + .wrapper-white to get white background -->
<!-- ./content wrapper -->