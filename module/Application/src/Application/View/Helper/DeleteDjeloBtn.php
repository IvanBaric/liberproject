<?php
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

class DeleteDjeloBtn extends AbstractHelper {

	public function __invoke($delete_url=false, $id=false, $text=false, $orphanCheck=false, $orphanErrorMsg="") {

		$text = (!empty($text)) ? $text : "Izbriši iz knjižničnog kataloga";
		$orphanErrorMsg = $orphanErrorMsg['delete']['deleteOrphans'];

		//ima unosa u drugoj tablici
		if($orphanCheck > 0) {
			$output = "<a href='#' class='btn btn-primary btn-clean btn-rounded pull-right orphanError' data-message='{$orphanErrorMsg}'><i class='fa fa-times' aria-hidden='true'></i> {$text}</a>";
		}
		else {
			$output = "<a href='{$delete_url}' role='button' class='btn btn-primary btn-clean btn-rounded pull-right confirmDelete'><i class='fa fa-remove'></i> {$text}</a>";
		}

		return $output;
	}
	
}


