<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RenderFormElement extends AbstractHelper {

    public function __invoke($element) {

        // FORM ROW
        $html = '<div class="form-group">';

        // LABEL
        $html .= '<label class="form-label" for="' . $element->getAttribute('id') . '">' . $element->getLabel() . '</label>';

        // ELEMENT
        /*
         - Check if element has error messages
         - If it does, add my error-class to the element's existing one(s),
           to style the element differently on error
        */
        if (count($element->getMessages()) > 0) {
            $classAttribute = ($element->hasAttribute('class') ? $element->getAttribute('class') . ' ' : '');
            $classAttribute .= 'input-error';
            /*
             * Normally, here I would have added a space in my string (' input-error')
             * Logically, I would figure that if the element already has a class "cls"
             * and I would do $element->getAttribute('class') . 'another-class'
             * then the class attribute would become "clsanother-class"
             * BUT it seems that, even if I don't intentionally add a space in my string,
             * I still get "cls another-class" as the resulted concatenated string
             * I assume that when building the form, ZF2 automatically
             * adds spaces after attributes values? so you/it won't have to
             * consider that later, when you'd eventually need to add another
             * value to an attribute?
             */
            $element->setAttribute('class', $classAttribute);
        }
        $html .= $this->view->formElement($element);
        /*
         * Of course, you could decide/need to do things differently,
         * depending on the element's type

           switch ($element->getAttribute('type')) {
               case 'text':
               case 'email': {

                   break;
               }
               default: {

               }
           }
         */
        // ERROR MESSAGES
        // Custom class (.form-validation-error) for the default html wrapper - <ul>
        $html .= $this->view->FormElementErrors($element, array('class' => 'form-validation-error'));

        $html .= '</div>'; # /.form-group
        $html .= '<div class="clearfix" style="height: 15px;"></div>';

        return $html . PHP_EOL;


    }

}