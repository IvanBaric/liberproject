<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Header extends AbstractHelper {

	// poziva se prije css klase "innerLR"
	public function __invoke($naslov_stranice="Naslov stranice", $podnaslov="Podnaslov", $btn_text=false, $defaultRoute, $newAction, $icon="plus") {

		$urlHelper = $this->view->plugin('url');
		
	
		$newAction = (!$newAction) ? array('action'=>'edit')  :  array('action' =>$newAction);
		
		$new_url = ($defaultRoute) ? $urlHelper($defaultRoute, $newAction) : FALSE;



		$output  = "<div class='page-title'>";

		if(!empty($btn_text) && !empty($new_url)){
			// dodaj novi entitet
			$output .= "<a class='btn btn-primary pull-right' href='{$new_url}'><i class='fa fa-{$icon}' aria-hidden='true'></i> {$btn_text}</a>";
		} else {
			// traži

			$output .= " <div class='input-group pull-right col-md-3'>
							<span class='input-group-addon'><i class='fa fa-search'></i></span>
							<input class='form-control searchText' type='text'>
							<div class='input-group-btn'>
								<button class='btn btn-default search'>Traži</button>
							</div>
						</div>
			";
		}

		$output .= "<h1><i class='fa fa-{$icon}' aria-hidden='true'></i> {$naslov_stranice}</h1>";
		$output .= "<p>{$podnaslov}</p>";




		$output .= "<div class='clearfix'></div>";
		$output .= "</div>";
		$output .= "<div class='separator bottom'></div>";
	
		return $output;
	}
}

#$output .= "<div class='input-append'><input class='span2 search' id='appendedInputButton' type='text'><button class='btn' type='button'>Go!</button></div>";