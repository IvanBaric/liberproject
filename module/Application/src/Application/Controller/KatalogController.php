<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;

class KatalogController extends MyController
{

//$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['edit']['success']);
//return $this->redirect()->toRoute($this->settings->redirect['edit']['success'], array('action' =>'edit', 'id'=> $id));

    public function noviunosAction()
    {
        $id = $this->params()->fromRoute('id');
        // pronađi djelo u katalogu
        $kataloskoDjelo = $this->getEntityManager()->getRepository('Application\Entity\Djelo')->findOneBy(array('id' => $id));
        


        if(!$kataloskoDjelo){
            $this->flashMessenger()->setNamespace('success')->addMessage('U katalogu ne postoji traženo književno djelo');
            return $this->redirect()->toRoute('knjiznicni-katalog');
        }


        
        //postoji li djelo u knjiznicnom katalogu?
        $knjiznicnoDjelo = $this->getRepository()->findBy(array('djelo' => $id));

        if($knjiznicnoDjelo){
            $this->flashMessenger()->setNamespace('success')->addMessage('Književno djelo se već nalazi u Mojem katalogu '. $knjiznicnoDjelo['naziv']);
            return $this->redirect()->toRoute('knjiznicni-katalog');
        }


        
        if(!$knjiznicnoDjelo){
            $knjiznicniKatalog = $this->getEntity();
            $knjiznicniKatalog->setDjelo($kataloskoDjelo);
            $this->getEntityManager()->persist($knjiznicniKatalog);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->setNamespace('success')->addMessage($kataloskoDjelo->getNaziv() . " je uspješno dodano u knjižnični katalog");
            return $this->redirect()->toRoute('knjiznicni-katalog/default', array('action' => 'id', 'id' =>$id));
        }

        die();
    }


    public function detaljiAction(){
        $nadjeni = false;

        //mora postojati id koji treba biti ispravan
        $id = $this->params()->fromRoute('id');
        


        // nalazi li se djelo u knjižničnom katalogu?
        $djeloEntity = $this->getEntityManager()->getRepository('Application\Entity\Katalog')->findOneBy(array('djelo' => $id));




        $kataloskiBrojeviDjela = $this->getEntityManager()->getRepository('Application\Entity\Katalog')->getKataloskiBrojeviDjela($id);

        $model = $this->getTable();

        $countKataloskihBrojeva = $model->countKataloskiBrojevi($kataloskiBrojeviDjela);
        

        




        if(!$djeloEntity){ die("asdasd");}



        $form = new \Application\Form\KataloskiBrojForm($this->getEntityManager());

        if ($this->getRequest()->isPost()) {

            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) { return new ViewModel(array('form' => $form, 'djelo' => $djeloEntity->getDjelo(), 'paginator' => $kataloskiBrojeviDjela, 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasOrphans' => $countKataloskihBrojeva)); }




            // kataloskiBrojevi iz forme
            $kataloskiBrojevi = $model->getArrayFromFormData($form->getData());

            //  svi kataloški brojevi koji postoje
            $sviKataloskiBrojevi = $this->getEntityManager()->getRepository('Application\Entity\KataloskiBroj')->fetchAll();
            $sviKataloskiBrojeviArray = $model->fetchKataloskiBrojevi($sviKataloskiBrojevi);


            // različiti kataloški brojevi - za unijeti u bazu
            $arrayDiff = array_diff($kataloskiBrojevi, $sviKataloskiBrojeviArray);
            $saved = $model->save($arrayDiff, $djeloEntity);


            // kataloški brojevi koji su unijeti u bazu podataka
           if($saved){
               foreach($saved as $s){
                   $this->flashMessenger()->setNamespace('info')->addMessage('Uspješno dodan kataloški broj: ' . $s->getKataloskiBroj());
               }
           }


            // kataloški brojevi koji se ne unose, već se ispisuje samo poruka da se ne mogu unijeti...
            $arrayIntersect = array_intersect($kataloskiBrojevi, $sviKataloskiBrojeviArray);
            if(is_array($arrayIntersect)){
                foreach($arrayIntersect as $dupliUnosi){
                    $this->flashMessenger()->setNamespace('error')->addMessage('dupli unosi: '. $dupliUnosi);
                }
            }

            return $this->redirect()->toRoute('knjiznicni-katalog/default', array('action' => 'id', 'id' => $id));

        } // endPost


        return new ViewModel(array('form' => $form, 'paginator' => $kataloskiBrojeviDjela, 'djelo' => $djeloEntity->getDjelo(), 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasOrphans' => $countKataloskihBrojeva));
    }


    public function confirmDeleteAction(){
        $id = $this->params()->fromRoute('id');


        $kataloskoDjelo = $this->getEntityManager()->getRepository('Application\Entity\Katalog')->findOneBy(array('id' => $id));

        if(!$kataloskoDjelo){
            return $this->redirect()->toRoute('error');
        }
        
        return new ViewModel(array('djelo' => $kataloskoDjelo));
        
//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($kataloskoDjelo); die();
    }



}
