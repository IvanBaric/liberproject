<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class DjeloController extends MyController
{

    //TODO: napraviti refactor

    public function editAction(){
        $id = $this->params()->fromRoute('id');
        if($id){$id = $this->decodeHash($id);}

        $table = $this->getTable();
        $form = $this->getForm();
        $entity = $this->getEntity();
        $crud = $this->getCrud();


        $form->bind($entity);

        // Process
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) {return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert']));}

            $form->bind($entity);
            
//            echo "<pre>";
//            \Doctrine\Common\Util\Debug::dump($entity); die();

            $naziv = $entity->getNaziv();
            $izdavacId = $entity->getIzdavac()->getId();
            $godinaIzdanja = $entity->getGodinaIzdanja();
            $autori = $entity->getAutori();
            $medijId = $entity->getMedij()->getId();
            $knjizevnaVrstaId = $entity->getKnjizevnaVrsta()->getId();



            if($id){
                $entity = $this->getEntityManager()->getRepository('Application\Entity\Djelo')->findOneBy(array('id' => $id));
                if(!$entity){return $this->redirect()->toRoute('error');}


                $autoriBaza = $entity->getAutori();

                foreach($autoriBaza as $autorBaza){
                    $autorBaza->removeDjelo($entity);


                    $this->getEntityManager()->persist($autorBaza);
                    $this->getEntityManager()->flush();

                }




            } else {
                $postojiLiDjeloUKatalogu = $this->getEntityManager()->getRepository('Application\Entity\Djelo')->findOneBy(
                    array(
                        'naziv' => $naziv,
                        'izdavac' => $izdavacId,
                        'godinaIzdanja' => $godinaIzdanja
                    ));

                if ($postojiLiDjeloUKatalogu) {
                    $this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
                    return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
                }

                $entity = new \Application\Entity\Djelo();
            }





            $entity->setNaziv($naziv);
            $entity->setGodinaIzdanja($godinaIzdanja);

            $izdavac = $this->getEntityManager()->getRepository('Application\Entity\Izdavac')->findOneBy(array('id' => $izdavacId));
            $entity->setIzdavac($izdavac);

            $medij = $this->getEntityManager()->getRepository('Application\Entity\Medij')->findOneBy(array('id' => $medijId));
            $entity->setMedij($medij);

            $knjizevnaVrsta = $this->getEntityManager()->getRepository('Application\Entity\KnjizevnaVrsta')->findOneBy(array('id' => $knjizevnaVrstaId));
            $entity->setKnjizevnaVrsta($knjizevnaVrsta);

            
//onemogućava dupli unos autora za jedno djelo            
            if($autori) {
                $autoriArray = array();
                foreach ($autori as $autor) {
                    array_push($autoriArray, $autor->getId());
                }

                $autor_unique = array_unique($autoriArray);

                foreach ($autor_unique as $autorId) {
                    $autor = $this->getEntityManager()->getRepository('Application\Entity\Autor')->findOneBy(array('id' => $autorId));

                    $entity->setAutor($autor);
                }
            }


            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();
            $unos = true;


            // POST - edit

            $action = ($id) ? "edit" : "insert";


                if($unos){
                    $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message[$action]['success']);
                    return $this->redirect()->toRoute($this->settings->redirect[$action]['success'], array('action' =>$action, 'id'=> $id));
                } else {
                    return $this->redirect()->toRoute($this->settings->redirect[$action]['error']);
                }
            }






        //ako nije post
        else {


            // GET - edit - ucitaj podatke iz baze u formu
            if($id){
                $form->get('submit')->setValue($this->settings->submitButtonText['edit']);
                $entity->setId($id);
                $entity = $crud->find($entity);

                // provjerava postoji li entity i je li to user entity
//					$entity = $this->beforeEditCheckUserEntity($entity);


                $form->setHydrator(new DoctrineHydrator($this->getEntityManager(), false));
                #$form->setObject(new \Application\Entity\Clan());

//					echo "<pre>";
//					\Doctrine\Common\Util\Debug::dump($entity); die();

                $form->bind($entity);

                return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['edit'], 'entity' => $entity));

            }

            // GET - inace je insert
            $form->get('submit')->setValue($this->settings->submitButtonText['insert']);

            return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert']));
        }

    }




    public function ajaxSearchAction(){

        // popis svih id-ova u Mojem katalogu
        $ids = $this->getEntityManager()->getRepository('Application\Entity\Katalog')->getAllIds();

        $allIds = array();
        foreach($ids as $id){
            array_push($allIds, $id->getDjelo()->getId());
        }

        
        $repository = $this->getRepository();
        
        $sortParams = $this->checkSortParams();

        $results = $repository->fetchAll($sortParams);

        $paginator = $this->zendPaginator($results, $sortParams);


        return new JsonModel (array('paginator' => $paginator, 'sortParams' => $sortParams, 'route' => $this->settings->route, 'message' => $this->settings->message, 'ids'=> $allIds));
    }



    public function confirmDeleteAction(){
        $id = $this->params()->fromRoute('id');

        $djelo = $this->getEntityManager()->getRepository('Application\Entity\Djelo')->findOneBy(array('id' => $id));

        if(!$djelo){
            return $this->redirect()->toRoute('error');
        }

        return new ViewModel(array('djelo' => $djelo));
    }





    public function deleteAction(){
        // deleteAction ne može se pristupiti izravnim upisom adrese u preglednik
        $referer = $this->getRequest()->getHeader('Referer');
        if(!$referer) {return $this->redirect()->toRoute('error');}

        $token = $this->params()->fromRoute('token');
        if(!$token) {return $this->redirect()->toRoute('error');};


        $id = $this->params()->fromRoute('id'); //114
        if(!$id) {return $this->redirect()->toRoute('error');};
        $id = $this->decodeHash($id);

        $entity = $this->getEntity();
        $entity->setId($id);

        $crud = $this->getCrud();
        $entity = $crud->find($entity);

        if(!$entity) {return $this->redirect()->toRoute('error');}



//		if($table->beforeDeleteCheckUserEntity){
//			if(!$crud->isAdmin()){
//				if(!$this->checkUserEntity($entity)){
//					$this->flashMessenger()->setNamespace('error')->addMessage($table->message['delete']['notUserEntity']);
//					return $this->redirect()->toRoute($table->redirect['delete']['error']);
//				}
//			}
//		}







        if($this->hasOrphans($entity)){
            $this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['delete']['deleteOrphans']);

            if($this->settings->afterDeleteRedirectToPreviousPage == TRUE){
                $referer = $this->getRequest()->getHeader('Referer');
                return ($referer) ? $this->redirect()->toUrl($referer->getUri()) : $this->redirectToErrorPage();
            } else {
                return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
            }

        }


        if($crud->delete($entity)){
            $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['delete']['success']);

            if($this->settings->afterDeleteRedirectToPreviousPage == TRUE){
                $referer = $this->getRequest()->getHeader('Referer');
                return ($referer) ? $this->redirect()->toUrl($referer->getUri()) : $this->redirectToErrorPage();

            } else {
                return $this->redirect()->toRoute($this->settings->redirect['delete']['success']);
            }


        } else {

            return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
        }

    }
    
    
    
    
}
