<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class RazredController extends MyController
{

    public function index2Action(){

        $hasPodrucniOdjel = FALSE;
        $podrucniOdjel = $this->getEntityManager()->getRepository('Application\Entity\PodrucniOdjel')->findAll();
        if(count($podrucniOdjel)>0){
            $hasPodrucniOdjel = TRUE;
        }




        $form = $this->getForm();


        if ($this->getRequest()->isPost()) {
            $crud = $this->getCrud();
            $entity = $this->getEntity();
            $repository = $this->getRepository();
            $form->bind($entity);

            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) {
                return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasPodrucniOdjel' => $hasPodrucniOdjel));
            }


            if($this->settings->recordExists) {
                if ($repository->recordExists($entity)) {
                    $this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
                    return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasPodrucniOdjel' => $hasPodrucniOdjel));
                }
            }

            if($crud->insert($entity)){
                $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['insert']['success']);
                return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit'));
            } else {
                return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
            }
        }

        return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasPodrucniOdjel' => $hasPodrucniOdjel));

    }


    public function ajaxSearchAction(){

        $hasPodrucniOdjel = FALSE;
        $podrucniOdjel = $this->getEntityManager()->getRepository('Application\Entity\PodrucniOdjel')->findAll();


        if(count($podrucniOdjel)>0){


            $hasPodrucniOdjel = TRUE;
        }


        $repository = $this->getRepository();

        $ajaxSearchParams = $this->checkAjaxSearchParams();
        $sortParams = $this->checkSortParams();



        $results = $repository->fetchAll($sortParams);
        $paginator = $this->zendPaginator($results, $sortParams);


        return new JsonModel (array('paginator' => $paginator, 'sortParams' => $sortParams, 'route' => $this->settings->route, 'message' => $this->settings->message, 'hasPodrucniOdjel' => $hasPodrucniOdjel));
    }


    public function editAction(){
        $id = $this->params()->fromRoute('id');
        if($id){$id = $this->decodeHash($id);}

        $hasPodrucniOdjel = FALSE;
        $podrucniOdjel = $this->getEntityManager()->getRepository('Application\Entity\PodrucniOdjel')->findAll();
        if(count($podrucniOdjel)>0){
            $hasPodrucniOdjel = TRUE;
        }

        #$table = $this->getTable();
        $form = $this->getForm();
        $entity = $this->getEntity();
        $repository = $this->getRepository();
        $crud = $this->getCrud();


        $form->bind($entity);


        // Process
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) {
                return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert'], 'hasPodrucniOdjel' => $hasPodrucniOdjel));}


            // POST - edit
            if($id){
                $entity->setId($id);

                if(!$entity){
                    die("dodati error page");
                }

                if($this->settings->recordExists) {
                    if ($repository->recordExists($entity)) {
                        $this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
                        return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
                    }
                }

                if($newData = $crud->edit($entity)){
                    $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['edit']['success']);
                    return $this->redirect()->toRoute($this->settings->redirect['edit']['success'], array('action' =>'edit', 'id'=> $id));
                } else {
                    return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
                }



            }
            // POST -insert
            else{
                if($this->settings->recordExists) {
                    if ($repository->recordExists($entity)) {
                        $this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
                        return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
                    }
                }

                if($crud->insert($entity)){
                    $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['insert']['success']);
                    return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit'));
                } else {
                    return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
                }
            }



        } // end if->post

        //ako nije post
        else {


            // GET - edit - ucitaj podatke iz baze u formu
            if($id){
                $form->get('submit')->setValue($this->settings->submitButtonText['edit']);
                $entity->setId($id);
                $entity = $crud->find($entity);

                // provjerava postoji li entity i je li to user entity
//					$entity = $this->beforeEditCheckUserEntity($entity);


                $form->setHydrator(new DoctrineHydrator($this->getEntityManager(), false));
                #$form->setObject(new \Application\Entity\Clan());

//					echo "<pre>";
//					\Doctrine\Common\Util\Debug::dump($entity); die();

                $form->bind($entity);

                return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['edit'], 'entity' => $entity, 'hasPodrucniOdjel' => $hasPodrucniOdjel));

            }

            // GET - inace je insert
            $form->get('submit')->setValue($this->settings->submitButtonText['insert']);

            return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert'], 'hasPodrucniOdjel' => $hasPodrucniOdjel));
        }

    }

}
