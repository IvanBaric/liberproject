<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class PosudbaController extends MyController
{

    public function posudbaAction(){
        $form = $this->getForm();
        $entity = $this->getEntity();

        if ($this->getRequest()->isPost()) {
//echo "<pre>";
//\Doctrine\Common\Util\Debug::dump($this->getRequest()->getPost()); die();

            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) {return new ViewModel(array('form' => $form));}
            
            $formData = $form->getData();



            $isError = FALSE;

            // pronađi člana
            $clan = $this->getEntityManager()->getRepository('Application\Entity\Ucenik')->find($formData['clan']);
            if(!$clan instanceof \Application\Entity\Ucenik){
                $isError = true;
                $this->flashMessenger()->setNamespace('error')->addMessage("Ne postoji član s navedenim id-om");
            }

            // pronađi kataloški broj
            $kataloskiBroj = $this->getEntityManager()->getRepository('Application\Entity\KataloskiBroj')->findOneBy(array('kataloskiBroj' => $formData['kataloskiBroj']));
            if(!$kataloskiBroj instanceof \Application\Entity\KataloskiBroj){
                $isError = true;
                $this->flashMessenger()->setNamespace('error')->addMessage("Kataloški broj ne postoji");
            } else {
                $idKataloskiBroj = $kataloskiBroj->getId();
            }

            // provjeri je li član posudio kataloški broj,
            // AKO JE - učenik je posudio knjigu i želi ju vratiti ili produljiti posudbu
            if($clan && $kataloskiBroj){
                $postojiLiPosudba = $this->getEntityManager()->getRepository('Application\Entity\Posudba')->findOneBy(array('clan' => $clan->getId(), 'kataloskiBroj' => $idKataloskiBroj, 'updatedAt' => NULL));

                if($postojiLiPosudba){

                    if($formData['produljiPosudbu'] == 1){
                        $dateTime = new \DateTime('+2weeks');
                        $format = $dateTime->format("d.m.Y");
                        $postojiLiPosudba->produljiPosudbu();
                        $this->flashMessenger()->setNamespace('info')->addMessage("Produžili ste posudbu do datuma: ". $format);

                    } else {
                        $postojiLiPosudba->setUpdatedAt();
                        $this->flashMessenger()->setNamespace('info')->addMessage("Književno djelo je vraćeno u knjižnicu");
                    }

                    $this->getEntityManager()->persist($postojiLiPosudba);
                    $this->getEntityManager()->flush();
                    return $this->redirect()->toRoute('posudba/posudba');

                }
            }


            if($clan && $kataloskiBroj) {
                // AKO NIJE - učenik želi posuditi kataloški broj
                $posudba = $this->getEntityManager()->getRepository('Application\Entity\Posudba')->findOneBy(array('kataloskiBroj' => $idKataloskiBroj, 'updatedAt' => NULL));
                if ($posudba instanceof \Application\Entity\Posudba) {
                    $isError = true;
                    $this->flashMessenger()->setNamespace('error')->addMessage("Kataloški broj se nalazi u aktivnoj posudbi");
                }
            }

            if($isError){
                return new ViewModel(array('form' => $form));
            }

            
            
//            $hydrator = new DoctrineHydrator($this->getEntityManager());
//            $entity = $hydrator->hydrate($form->getData(), $entity);

            $posudba = new \Application\Entity\Posudba();
            $posudba->setClan($clan);
            $posudba->setKataloskiBroj($kataloskiBroj);

            $this->getEntityManager()->persist($posudba);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->setNamespace('success')->addMessage("Uspješno ste posudili književno-umjetničko djelo");
            return $this->redirect()->toRoute('posudba/posudba');
            //return new ViewModel(array('form' => $form));

        }
        
        return new ViewModel(array('form' => $form));
    }

}
