<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;

class AlatController extends MyController
{

    public function upisUVisiRazredAction(){
        if ($this->getRequest()->isPost()) {
            $popisSvihRazreda = $this->getEntityManager()->getRepository('Application\Entity\Razred')->findAll();

            if($popisSvihRazreda){
                foreach($popisSvihRazreda as $razredObj){
                    $razred = $razredObj->getRazred();
                    $odjel = ($razredObj->getOdjel()) ? $razredObj->getOdjel() : NULL;
                    $podrucniOdjel = ($razredObj->getPodrucniOdjel()) ? $razredObj->getPodrucniOdjel() : NULL;
                    $podrucniOdjelGodineSkolovanja = ($razredObj->getPodrucniOdjel()) ? $razredObj->getPodrucniOdjel()->getGodineSkolovanja() : NULL;


                    // osmi razredi
                    if($razred == 8 || $razred == 0){
                        $razredObj->setRazred(NULL);
                        $this->getEntityManager()->persist($razredObj);
                        continue;
                    }

                    // upis u viši razred učenika područnih odjela
                    if($podrucniOdjel){
                        if($razred == $podrucniOdjelGodineSkolovanja){
                            $razredObj->setPodrucniOdjel(NULL);
                        }
                    }

                    //svi učenici idu u zajednički 5. razred
                    if($razred == 4){
                        if($odjel){
                            $razredObj->setOdjel("");
                        }
                    }

                    $razredObj->prebaciUVisiRazred();

                    $this->getEntityManager()->persist($razredObj);
                }
                $this->getEntityManager()->flush();
            }
        }

        return new ViewModel(array());
    }
    
    
    public function postavkeKnjizniceAction(){
        return new ViewModel(array());
    }
	
}
