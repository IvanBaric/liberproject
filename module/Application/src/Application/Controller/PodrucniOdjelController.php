<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;

class PodrucniOdjelController extends MyController
{
    public function indexAction(){

        $form = $this->getForm();


        if ($this->getRequest()->isPost()) {
            $crud = $this->getCrud();
            $entity = $this->getEntity();
            $form->bind($entity);

            $form->setData($this->getRequest()->getPost());

            if (!$form->isValid()) {
                return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
            }

            if($crud->insert($entity)){
                $this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['insert']['success']);
                return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit'));
            } else {
                return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
            }
        }

        return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));

    }
}
