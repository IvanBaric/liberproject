<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class KatalogSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }


    public $entityRoute = "knjiznicni-katalog";

    public $afterDeleteRedirectToPreviousPage = FALSE;
}