<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class DjeloSettings extends MySettings {
    public $entityRoute = "liber-katalog";

    public $defaultTableOrderByColumn = 'dj.id';

    public $redirect = array(
        'insert' => array(
            'success' => 'liber-katalog/uspjesan-unos',
            'error' => ''
        ),

        'edit' => array(
            'success' => 'liber-katalog',
            'error' => ''
        ),

        'delete' => array(
            'success' => 'liber-katalog',
            'error' => 'liber-katalog'
        ),
    );


    public $pageHeading = array(
        'insert' => "Unesi novo književno-umjetničko djelo",
        'edit' => "Uredi književno-umjetničko djelo"
    );
}