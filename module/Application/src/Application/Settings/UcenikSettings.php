<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class UcenikSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "ucenici";

    public $recordExists = true;

    public $pageHeading = array(
        'insert' => "Unos novoga učenika",
        'edit' => "Uredi učenika"
    );
}