<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class DjelatnikSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "djelatnici";

    public $pageHeading = array(
        'insert' => "Unesi novog djelatnika",
        'edit' => "Uredi djelatnika"
    );
}