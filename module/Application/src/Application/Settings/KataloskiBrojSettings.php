<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class KataloskiBrojSettings extends MySettings {
    public $entityRoute = "kataloski-broj";

    public $afterDeleteRedirectToPreviousPage = true;

    public $redirect = array(
        'insert' => array(
            'success' => 'kataloski-broj',
            'error' => ''
        ),

        'edit' => array(
            'success' => 'kataloski-broj',
            'error' => ''
        ),

        'delete' => array(
            'success' => 'knjiznicni-katalog',
            'error' => 'knjiznicni-katalog'
        ),
    );

}