<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class KnjizevnaVrstaSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }


    public $entityRoute = "liber-katalog/knjizevne-vrste";

    public $recordExists = true;

    public $pageHeading = array(
        'insert' => "Unesi novu književnu vrstu",
        'edit' => "Uredi književnu vrstu"
    );
}