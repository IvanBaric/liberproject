<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class RazredSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "ucenici/razredi";
    public $recordExists = true;

    public $pageHeading = array(
        'insert' => "Unesi novi razred",
        'edit' => "Uredi razred"
    );
}