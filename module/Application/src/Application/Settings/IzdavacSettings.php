<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class IzdavacSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "liber-katalog/izdavaci";

    public $recordExists = true;


    public $pageHeading = array(
        'insert' => "Unesi novog izdavača",
        'edit' => "Uredi izdavača"
    );
}