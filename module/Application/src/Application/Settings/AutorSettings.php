<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class AutorSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $recordExists = false;

    public $entityRoute = "liber-katalog/autori";

    public $pageHeading = array(
        'insert' => "Unesi novog autora",
        'edit' => "Uredi autora"
    );
}