<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class PosudbaSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "posudba";

}