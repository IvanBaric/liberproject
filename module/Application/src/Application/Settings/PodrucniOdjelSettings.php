<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class PodrucniOdjelSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "podrucni-odjeli";

    public $pageHeading = array(
        'insert' => "Unesi novi područni odjel",
        'edit' => "Uredi područni odjel"
    );
}