<?php
namespace Application\Settings;
use \MyModule\Settings\MySettings;

class MedijSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    public $entityRoute = "liber-katalog/mediji";

    public $recordExists = true;

    public $pageHeading = array(
        'insert' => "Unesi novi medij",
        'edit' => "Uredi medij"
    );
}