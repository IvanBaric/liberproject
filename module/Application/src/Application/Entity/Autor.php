<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyModule\Entity\MyEntity;
use Doctrine\Common\Collections\Collection as Collection;
/**
 * @ORM\Entity(repositoryClass="\Application\Repository\AutorRepository")
 * @ORM\Table(name="katalog_autor", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Autor extends MyEntity {

    public function __construct(){
        $this->djela = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="ime", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $ime;


    /**
     * @var string
     *
     * @ORM\Column(name="prezime", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $prezime;


    /**
     * @var datetime
     *
     * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $createdAt;


    /**
     * @var datetime
     *
     * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $updatedAt;




    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Djelo", inversedBy="autori", cascade={"persist"})
     * @ORM\JoinTable(name="katalog_autor_djelo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="autor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="djelo_id", referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     *   }
     * )
     */
    protected $djela;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIme()
    {
        return $this->ime;
    }

    /**
     * @param string $ime
     */
    public function setIme($ime)
    {
        $this->ime = $ime;
    }

    /**
     * @return string
     */
    public function getPrezime()
    {
        return $this->prezime;
    }

    /**
     * @param string $prezime
     */
    public function setPrezime($prezime)
    {
        $this->prezime = $prezime;
    }




    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return \Application\Entity\Djelo
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        if($this->createdAt instanceof \DateTime){
            $date = $this->createdAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return \Application\Entity\Djelo
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(){
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(){
        if($this->updatedAt instanceof \DateTime){
            $date = $this->updatedAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }
    }



public function setDjelo($djelo){
    $this->djela->add($djelo);
}

    public function removeDjelo($djelo){
        $this->djela->removeElement($djelo);
    }


    /**
     * Add djelo
     *
     * @param Collection
     */
    public function addDjela($djela)
    {
        if($djela) {
            foreach ($djela as $d) {
                $d->addAutori($this);
                $this->djela->add($d);
            }
        }
    }

    /**
     * Remove djelo
     *
     * @param Collection
     */
    public function removeDjela($djela)
    {
        foreach ($djela as $d) {
            $d->setAutor(NULL);
            $this->djela->removeElement($d);
        }
    }

    /**
     * Get djelo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDjela()
    {
        return $this->djela;
    }

    

    
   

}