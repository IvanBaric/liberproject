<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyModule\Entity\MyEntity;
use Doctrine\Common\Collections\Collection;
/**
 * @ORM\Entity(repositoryClass="\Application\Repository\PodrucniOdjelRepository")
 * @ORM\Table(name="clan_podrucni_odjel", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class PodrucniOdjel extends MyEntity {

    public function __construct(){
            $this->razredi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="naziv", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $naziv;


     /**
      * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
      *
      * @ORM\OneToMany(targetEntity="Application\Entity\Razred", mappedBy="podrucniOdjel")
      */
    protected $razredi;


    /**
     * @var integer
     *
     * @ORM\Column(name="godine_skolovanja", type="integer", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $godineSkolovanja;


//    /**
//     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
//     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
//     */
//    protected $user;

//    public function setUser(\Application\Entity\User $user){
//        $this->user = $user;
//    }
//
//    public function getUser(){
//        return $this->user;
//    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param string $naziv
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;
    }


    /**
     * Add razredi
     *
     * @param Collection $razredi
     */
    public function addRazredi(Collection $razredi)
    {
        foreach ($razredi as $razred) {
            $razred->setPodrucniOdjel($this);
            $this->razredi->add($razred);
        }
    }

    /**
     * Remove razredi
     *
     * @param Collection $razredi
     */
    public function removeRazredi(Collection $razredi)
    {
        foreach ($razredi as $razred) {
            $razred->setPodrucniOdjel($this);
            $this->razredi->removeElement($razred);
        }
    }

    /**
     * Get Collection
     *
     * @return Collection
     */
    public function getRazredi()
    {
        return $this->razredi;
    }

    /**
     * @return int
     */
    public function getGodineSkolovanja()
    {
        return $this->godineSkolovanja;
    }

    /**
     * @param int $godineSkolovanja
     */
    public function setGodineSkolovanja($godineSkolovanja)
    {
        $this->godineSkolovanja = $godineSkolovanja;
    }





}