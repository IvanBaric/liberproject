<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Application\Repository\KataloskiBrojRepository")
 * @ORM\Table(name="knjiznica_kataloski_broj", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class KataloskiBroj {

    public function __construct(){
        $this->posudbe = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="kataloski_broj", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $kataloskiBroj;



    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\Posudba", mappedBy="kataloskiBroj", cascade={"persist"})
     */
    private $posudbe;



    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Katalog", inversedBy="kataloskiBrojevi", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="katalog_id", referencedColumnName="id")})
     */
    private $katalog;




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getKataloskiBroj()
    {
        return $this->kataloskiBroj;
    }

    /**
     * @param int $kataloskiBroj
     */
    public function setKataloskiBroj($kataloskiBroj)
    {
        $this->kataloskiBroj = $kataloskiBroj;
    }





    /**
     * Add posudbe
     *
     * @param \Application\Entity\Posudba $posudbe
     * @return \Application\Entity\KataloskiBroj
     */
    public function addPosudba(\Application\Entity\Posudba $posudbe)
    {
        $this->posudbe[] = $posudbe;

        return $this;
    }

    /**
     * Remove posudbe
     *
     * @param \Application\Entity\Posudba $posudbe
     */
    public function removePosudba(\Application\Entity\Posudba $posudbe)
    {
        $this->posudbe->removeElement($posudbe);
    }

    /**
     * Get posudbe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosudba()
    {
        return $this->posudbe;
    }




    /**
     * Set katalog
     *
     * @param \Application\Entity\Katalog $katalog
     * @return \Application\Entity\KataloskiBrojevi
     */
    public function setKatalog(\Application\Entity\Katalog $katalog = null)
    {
        $this->katalog = $katalog;

        return $this;
    }

    /**
     * Get katalog
     *
     * @return \Application\Entity\Katalog
     */
    public function getKatalog()
    {
        return $this->katalog;
    }

    /**
     * Remove katalog
     *
     * @param \katalog $katalog
     */
    public function removeKatalog(\Application\Entity\Katalog $katalog)
    {
        $this->katalog->removeElement($katalog);
    }







}