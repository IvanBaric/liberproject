<?php

namespace Application\Entity;
use MyModule\Entity\MyEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection as Collection;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
/**
 * @ORM\Entity(repositoryClass="\Application\Repository\DjeloRepository")
 * @ORM\Table(name="katalog_djelo", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Djelo extends MyEntity{

    public function __construct(){
        $this->autori = new ArrayCollection();
        $this->katalozi = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="naziv", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $naziv;


    /**
     * @var string
     *
     * @ORM\Column(name="tip_skole", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $tipSkole;


    /**
     * @var integer
     *
     * @ORM\Column(name="godina_izdanja", type="integer", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $godinaIzdanja;


    /**
     * @var text
     *
     * @ORM\Column(name="materijalni_opis", type="text", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $materijalniOpis;


    /**
     * @var text
     *
     * @ORM\Column(name="napomena", type="text", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $napomena;


    /**
     * @var string
     *
     * @ORM\Column(name="ilustracija", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $ilustracija;



    /**
     * @var string
     *
     * @ORM\Column(name="udk1", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $udk1;


    /**
     * @var string
     *
     * @ORM\Column(name="udk2", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $udk2;


    /**
     * @var string
     *
     * @ORM\Column(name="udk3", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $udk3;


    /**
     * @var string
     *
     * @ORM\Column(name="isbn", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $isbn;

    /**
     * @var datetime
     *
     * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $createdAt;


    /**
     * @var datetime
     *
     * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $updatedAt;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_visible", type="boolean", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    protected $isVisible;




    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Izdavac", inversedBy="djela", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="izdavac_id", referencedColumnName="id")})
     */
    protected $izdavac;


    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\KnjizevnaVrsta", inversedBy="djela", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="knjizevna_vrsta_id", referencedColumnName="id")})
     */
    protected $knjizevnaVrsta;


    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Medij", inversedBy="djela", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medij_id", referencedColumnName="id")})
     */
    protected $medij;



    /**
     * @ORM\ManyToMany(targetEntity="Application\Entity\Autor", mappedBy="djela", cascade={"persist"})
     **/
    protected $autori; //mappedBy -> inverse side /owned side


    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\Katalog", mappedBy="djelo", cascade={"persist"})
     */
    protected $katalozi;



    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param string $naziv
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;
    }

    /**
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @return string
     */
    public function getTipSkole()
    {
        return $this->tipSkole;
    }

    /**
     * @param string $tipSkole
     */
    public function setTipSkole($tipSkole)
    {
        $this->tipSkole = $tipSkole;
    }

    /**
     * @return int
     */
    public function getGodinaIzdanja()
    {
        return $this->godinaIzdanja;
    }

    /**
     * @param int $godinaIzdanja
     */
    public function setGodinaIzdanja($godinaIzdanja)
    {
        $this->godinaIzdanja = $godinaIzdanja;
    }

    /**
     * @return text
     */
    public function getMaterijalniOpis()
    {
        return $this->materijalniOpis;
    }

    /**
     * @param text $materijalniOpis
     */
    public function setMaterijalniOpis($materijalniOpis)
    {
        $this->materijalniOpis = $materijalniOpis;
    }

    /**
     * @return text
     */
    public function getNapomena()
    {
        return $this->napomena;
    }

    /**
     * @param text $napomena
     */
    public function setNapomena($napomena)
    {
        $this->napomena = $napomena;
    }

    /**
     * @return string
     */
    public function getIlustracija()
    {
        return $this->ilustracija;
    }

    /**
     * @param string $ilustracija
     */
    public function setIlustracija($ilustracija)
    {
        $this->ilustracija = $ilustracija;
    }

    /**
     * @return string
     */
    public function getUdk1()
    {
        return $this->udk1;
    }

    /**
     * @param string $udk1
     */
    public function setUdk1($udk1)
    {
        $this->udk1 = $udk1;
    }

    /**
     * @return string
     */
    public function getUdk2()
    {
        return $this->udk2;
    }

    /**
     * @param string $udk2
     */
    public function setUdk2($udk2)
    {
        $this->udk2 = $udk2;
    }

    /**
     * @return string
     */
    public function getUdk3()
    {
        return $this->udk3;
    }

    /**
     * @param string $udk3
     */
    public function setUdk3($udk3)
    {
        $this->udk3 = $udk3;
    }



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return \Application\Entity\Autor
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        if($this->createdAt instanceof \DateTime){
            $date = $this->createdAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return \Application\Entity\Autor
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(){
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(){
        if($this->updatedAt instanceof \DateTime){
            $date = $this->updatedAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }
    }

    /**
     * @return boolean
     */
    public function isIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param boolean $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }







    /**
     * Set izdavac
     *
     * @param \Application\Entity\Izdavac $izdavac
     * @return \Application\Entity\Djelo
     */
    public function setIzdavac(\Application\Entity\Izdavac $izdavac = null)
    {
        $this->izdavac = $izdavac;

        return $this;
    }

    /**
     * Get izdavac
     *
     * @return \Application\Entity\Izdavac
     */
    public function getIzdavac()
    {
        return $this->izdavac;
    }

    /**
     * Remove izdavac
     *
     * @param \izdavac $izdavac
     */
    public function removeIzdavac(\Application\Entity\Izdavac $izdavac)
    {
        $this->izdavac->removeElement($izdavac);
    }


    /**
     * Set knjizevnaVrsta
     *
     * @param \Application\Entity\KnjizevnaVrsta $knjizevnaVrsta
     * @return \Application\Entity\KnjizevnaVrsta
     */
    public function setKnjizevnaVrsta(\Application\Entity\KnjizevnaVrsta $knjizevnaVrsta = null)
    {
        $this->knjizevnaVrsta = $knjizevnaVrsta;

        return $this;
    }

    /**
     * Get knjizevnaVrsta
     *
     * @return \Application\Entity\KnjizevnaVrsta
     */
    public function getKnjizevnaVrsta()
    {
        return $this->knjizevnaVrsta;
    }

    /**
     * Remove knjizevnaVrsta
     *
     * @param \Application\Entity\KnjizevnaVrsta $knjizevnaVrsta
     */
    public function removeKnjizevnaVrsta(\Application\Entity\KnjizevnaVrsta $knjizevnaVrsta)
    {
        $this->knjizevnaVrsta->removeElement($knjizevnaVrsta);
    }






    /**
     * Set medij
     *
     * @param \Application\Entity\Medij $medij
     * @return \Application\Entity\Medij
     */
    public function setMedij(\Application\Entity\Medij $medij = null)
    {
        $this->medij = $medij;

        return $this;
    }

    /**
     * Get medij
     *
     * @return \Application\Entity\Medij
     */
    public function getMedij()
    {
        return $this->medij;
    }

    /**
     * Remove medij
     *
     * @param \medij $medij
     */
    public function removeMedij(\Application\Entity\Medij $medij)
    {
        $this->medij->removeElement($medij);
    }




    /**
     * Add autori
     *
     * @param Collection $autori
     */
    public function addAutori( $autori)
    {
        foreach ($autori as $autor) {
            #$autor->addDjela($this);
            $this->autori->add($autor);
        }
    }

    /**
     * Remove autori
     *
     * @param Collection $autori
     */
    public function removeAutori( $autori)
    {
        foreach ($autori as $autor) {
            $autor->addDjela(null);
            $this->autori->removeElement($autor);
        }
    }

    /**
     * Get autori
     *
     * @return Collection
     */
    public function getAutori()
    {
        return $this->autori;
    }

    public function setAutor($autor)
    {

            $this->autori->add($autor);
            $autor->setDjelo($this);

    }


    public function popisAutora(){
        $autori = $this->getAutori();
        $count = count($autori);



        $result = "";

        if($autori){
            $i = 1;
            foreach($autori as $autor){
                if($count == $i){
                    $result .= $autor->getIme() . " " . $autor->getPrezime();
                } else {
                    $result .= $autor->getIme() . " " . $autor->getPrezime(). ", ";
                }

            $i++;
            }

            return $result;
        }
    }


    	/**
    	 * Add katalozi
    	 *
    	 * @param Collection
    	 */
    	public function addKatalog(Collection $katalozi)
    	{
            foreach ($katalozi as $katalog) {
                $katalog->setDjelo($this);
                $this->katalozi->add($katalog);
            }
    	}

    	/**
    	 * Remove katalozi
    	 *
    	 * @param Collection $katalozi
    	 */
    	public function removeKatalog(Collection $katalozi)
    	{
            foreach ($katalozi as $katalog) {
                $katalog->setDjelo($this);
                $this->katalozi->add($katalog);
            }
    	}

    	/**
    	 * Get katalozi
    	 *
    	 * @return Collection
    	 */
    	public function getKatalog()
    	{
    		return $this->katalozi;
    	}



}
