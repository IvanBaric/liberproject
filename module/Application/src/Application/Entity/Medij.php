<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Application\Repository\MedijRepository")
 * @ORM\Table(name="katalog_medij", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Medij {

    public function __construct(){
        $this->djela = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="naziv", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $naziv;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdAt;


    /**
     * @var datetime
     *
     * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    private $updatedAt;



    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\Djelo", mappedBy="medij", cascade={"persist"})
     */
    private $djela;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param string $naziv
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;
    }
    
    
        /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return \Application\Entity\Medij
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        if($this->createdAt instanceof \DateTime){
            $date = $this->createdAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return \Application\Entity\Medij
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(){
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(){
        if($this->updatedAt instanceof \DateTime){
            $date = $this->updatedAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }
    }




    	/**
    	 * Add djela
    	 *
    	 * @param \Application\Entity\Djelo $djela
    	 * @return \Application\Entity\Medij
    	 */
    	public function addDjelo(\Application\Entity\Djelo $djela)
    	{
    		$this->djela[] = $djela;

    		return $this;
    	}

    	/**
    	 * Remove djela
    	 *
    	 * @param \Application\Entity\Djelo $djela
    	 */
    	public function removeDjelo(\Application\Entity\Djelo $djela)
    	{
    		$this->djela->removeElement($djela);
    	}

    	/**
    	 * Get djela
    	 *
    	 * @return \Doctrine\Common\Collections\Collection
    	 */
    	public function getDjelo()
    	{
    		return $this->djela;
    	}




}