<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use \Application\Entity\Clan;
/**
 * @ORM\Entity(repositoryClass="\Application\Repository\UcenikRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ucenik extends Clan {



    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Razred", inversedBy="ucenici")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="razred_id", referencedColumnName="id")})
     */
    protected $razred;



    /**
     * Set razred
     *
     * @param \Application\Entity\Razred $razred
     * @return \Application\Entity\Clan
     */
    public function setRazred(\Application\Entity\Razred $razred = null)
    {
        $this->razred = $razred;

        return $this;
    }

    /**
     * Get razred
     *
     * @return \Application\Entity\Razred
     */
    public function getRazred()
    {
        return $this->razred;
    }

    /**
     * Remove razred
     *
     * @param \razred $razred
     */
    public function removeRazred(\Application\Entity\Razred $razred)
    {
        $this->razred->removeElement($razred);
    }
   

}