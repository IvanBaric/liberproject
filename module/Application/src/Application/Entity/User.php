<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyModule\Entity\MyEntity;
use ZfcUser\Entity\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class User extends MyEntity implements UserInterface
{
	/** 
	 * @ORM\Column(name="user_id",type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")  
	 */
    protected $id;

    /** @ORM\Column(name="firstname", type="string", nullable=true) */
    protected $firstname;
    
    /** @ORM\Column(name="lastname", type="string", nullable=true) */
    protected $lastname;
    
    /** @ORM\Column(name="username", type="string", nullable=true) */
    protected $username;

    /** @ORM\Column(name="email", type="string") */
    protected $email;

    /** @ORM\Column(name="display_name", type="string", nullable=true) */
    protected $displayName;

    /** @ORM\Column(name="password", type="string") */
    protected $password;

    /** @ORM\Column(name="state", type="string", nullable=true) */
    protected $state;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return UserInterface
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }
	
    
    public function getFirstname(){
    	return $this->firstname;
    }
    
    public function setFirstname($firstname){
    	return $this->firstname = $firstname;
    }
    
    public function getLastname(){
    	return $this->lastname;
    }
    
    public function setLastname($lastname){
    	return $this->lastname = $lastname;
    }
    


    
    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     * @return UserInterface
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     * @return UserInterface
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param string $password
     * @return UserInterface
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param int $state
     * @return UserInterface
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }



    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;

    public function setRole(\Application\Entity\Role $role){
        $this->role = $role;
    }

    public function getRole(){
        return $this->role;
    }



    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    protected $address;

    public function setAddress(\Application\Entity\Address $address){
        $this->address = $address;
    }

    public function getAddress(){
        return $this->address;
    }


}