<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Application\Repository\KatalogRepository")
 * @ORM\Table(name="knjiznica_katalog", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Katalog {

    public function __construct(){
        $this->kataloskiBrojevi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;




    /**
     * @var datetime
     *
     * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdAt;


    /**
     * @var datetime
     *
     * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    private $updatedAt;




    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Djelo", inversedBy="katalozi", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="djelo_id", referencedColumnName="id")})
     */
    private $djelo;


    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\KataloskiBroj", mappedBy="katalog", cascade={"persist"})
     */
    private $kataloskiBrojevi;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }




        /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return \Application\Entity\Autor
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        if($this->createdAt instanceof \DateTime){
            $date = $this->createdAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return \Application\Entity\Autor
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(){
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(){
        if($this->updatedAt instanceof \DateTime){
            $date = $this->updatedAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }
    }








    /**
     * Set djelo
     *
     * @param \Application\Entity\Djelo $djelo
     * @return \Application\Entity\Katalog
     */
    public function setDjelo(\Application\Entity\Djelo $djelo = null)
    {
        $this->djelo = $djelo;

        return $this;
    }

    /**
     * Get djelo
     *
     * @return \Application\Entity\Djelo
     */
    public function getDjelo()
    {
        return $this->djelo;
    }

    /**
     * Remove djelo
     *
     * @param \djelo $djelo
     */
    public function removeDjelo(\Application\Entity\Djelo $djelo)
    {
        $this->djelo->removeElement($djelo);
    }



    /**
     * Add kataloskiBrojevi
     *
     * @param \Application\Entity\KataloskiBroj $kataloskiBrojevi
     * @return \Application\Entity\Katalog
     */
    public function addKataloskiBroj(\Application\Entity\KataloskiBroj $kataloskiBroj)
    {
        $this->kataloskiBrojevi[] = $kataloskiBroj;

        return $this;
    }

    /**
     * Remove kataloskiBrojevi
     *
     * @param \Application\Entity\KataloskiBroj $kataloskiBrojevi
     */
    public function removeKataloskiBroj(\Application\Entity\KataloskiBroj $kataloskiBroj)
    {
        $this->kataloskiBrojevi->removeElement($kataloskiBroj);
    }

    /**
     * Get kataloskiBrojevi
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKataloskiBrojevi()
    {
        return $this->kataloskiBrojevi;
    }


}