<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_login", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class Login
{
    /**
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



    /**
     * @var datetime
     *
     * @ORM\Column(name="loggedIn", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $loggedIn;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }




    /**
     * Get loggedIn
     *
     * @return \DateTime
     */
    public function getLoggedIn(){
       return $this->loggedIn;

    }


    /**
     * Set loggedIn
     */
    public function setLoggedIn(){
        $this->loggedIn = new \DateTime();

        return $this;
    }




}