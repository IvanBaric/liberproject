<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyModule\Entity\MyEntity;
use Doctrine\Common\Collections\Collection;


/**
 * @ORM\Entity(repositoryClass="\Application\Repository\ClanRepository")
 * @ORM\Table(name="clan", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"ucenik" = "Application\Entity\Ucenik", "djelatnik" = "Application\Entity\Djelatnik"})
 */
abstract class Clan extends MyEntity {

	public function __construct(){
		$this->posudbe = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;



	/**
	 * @var string
	 *
	 * @ORM\Column(name="ime", type="string", precision=0, scale=0, nullable=false, unique=false)
	 */
	protected $ime;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="prezime", type="string", precision=0, scale=0, nullable=false, unique=false)
	 */
	protected $prezime;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="spol", length=1, type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
	 */
	protected $spol;


	/**
	 * @var text
	 *
	 * @ORM\Column(name="biljeska", type="text", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
	 */
	protected $biljeska;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="kontakt", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
	 */
	protected $kontakt;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="kontakt2", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
	 */
	protected $kontakt2;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
	 */
	protected $email;


	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
	 */
	protected $createdAt;


	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false)
	 */
	protected $updatedAt;


	/**
	 * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Application\Entity\Posudba", mappedBy="clan")
	 */
	protected $posudbe;


	/**
	 * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToOne(targetEntity="Application\Entity\Razred", inversedBy="ucenici")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="razred_id", referencedColumnName="id")})
	 */
	protected $razred;



	/**
	 * Set razred
	 *
	 * @param \Application\Entity\Razred $razred
	 * @return \Application\Entity\Clan
	 */
	public function setRazred(\Application\Entity\Razred $razred = null)
	{
		$this->razred = $razred;

		return $this;
	}

	/**
	 * Get razred
	 *
	 * @return \Application\Entity\Razred
	 */
	public function getRazred()
	{
		return $this->razred;
	}

	/**
	 * Remove razred
	 *
	 * @param \razred $razred
	 */
	public function removeRazred(\Application\Entity\Razred $razred)
	{
		$this->razred->removeElement($razred);
	}



	/**
	 * @ORM\ManyToOne(targetEntity="Application\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
	 */
	protected $user;

	public function setUser(\Application\Entity\User $user){
	    $this->user = $user;
	}

	public function getUser(){
	    return $this->user;
	}


	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return string
	 */
	public function getIme()
	{
		return $this->ime;
	}

	/**
	 * @param string $ime
	 */
	public function setIme($ime)
	{
		$this->ime = $ime;
	}

	/**
	 * @return string
	 */
	public function getPrezime()
	{
		return $this->prezime;
	}

	/**
	 * @param string $prezime
	 */
	public function setPrezime($prezime)
	{
		$this->prezime = $prezime;
	}

	/**
	 * @return string
	 */
	public function getSpol()
	{
		return $this->spol;
	}

	/**
	 * @param string $spol
	 */
	public function setSpol($spol)
	{
		$this->spol = $spol;
	}

	/**
	 * @return text
	 */
	public function getBiljeska()
	{
		return $this->biljeska;
	}

	/**
	 * @param text $biljeska
	 */
	public function setBiljeska($biljeska)
	{
		$this->biljeska = $biljeska;
	}

	/**
	 * @return string
	 */
	public function getKontakt()
	{
		return $this->kontakt;
	}

	/**
	 * @param string $kontakt
	 */
	public function setKontakt($kontakt)
	{
		$this->kontakt = $kontakt;
	}

	/**
	 * @return string
	 */
	public function getKontakt2()
	{
		return $this->kontakt2;
	}

	/**
	 * @param string $kontakt2
	 */
	public function setKontakt2($kontakt2)
	{
		$this->kontakt2 = $kontakt2;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}


	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 * @return \Application\Entity\Clan
	 * @ORM\PrePersist
	 */
	public function setCreatedAt()
	{
		$this->createdAt = new \DateTime();

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt(){
		if($this->createdAt instanceof DateTime){
			$date = $this->createdAt;
			$result = $date->format('Y-m-d H:i:s');

			if ($result) {
				return $result;
			} else { // format failed
				return "Unknown Time";
			}
		}

	}




	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 * @return \Application\Entity\Clan
	 * @ORM\PreUpdate
	 */
	public function setUpdatedAt(){
		$this->updatedAt = new \DateTime();

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt(){
		if($this->updatedAt instanceof \DateTime){
			$date = $this->updatedAt;
			$result = $date->format('Y-m-d H:i:s');

			if ($result) {
				return $result;
			} else { // format failed
				return "Unknown Time";
			}
		}
	}








	/**
	 * Add posudbe
	 *
	 * @param \Application\Entity\Posudba $posudbe
	 * @return \Application\Entity\Clan
	 */
	public function addPosudbe(Collection $posudbe)
	{
		foreach ($posudbe as $posudba) {
			$posudba->setClan($this);
			$this->posudbe->add($posudba);
		}
	}

	/**
	 * Remove posudbe
	 *
	 */
	public function removePosudbe(Collection $posudbe)
	{
		foreach ($posudbe as $posudba) {
			$posudba->setClan($this);
			$this->posudbe->removeElement($posudba);
		}
	}

	/**
	 * Get posudbe
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPosudbe()
	{
		return $this->posudbe;
	}



}