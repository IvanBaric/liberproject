<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyModule\Entity\MyEntity;
use Doctrine\Common\Collections\Collection;


/**
 * @ORM\Entity(repositoryClass="\Application\Repository\RazredRepository")
 * @ORM\Table(name="clan_razred", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class Razred extends MyEntity{

	public function __construct(){
		$this->ucenici = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="razred", type="integer", precision=0, scale=0, nullable=false, unique=false)
	 */
	protected $razred;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="odjel", type="string", precision=0, scale=0, nullable=true, unique=false)
	 */
	protected $odjel;


	/**
	 * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\ManyToOne(targetEntity="Application\Entity\PodrucniOdjel", inversedBy="razredi", cascade={"persist"})
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="podrucniOdjel_id", referencedColumnName="id")})
	 */
	protected $podrucniOdjel;


	/**
	 * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Application\Entity\Ucenik", mappedBy="razred", cascade={"persist"})
	 */
	protected $ucenici;

	/**
	 * @ORM\ManyToOne(targetEntity="Application\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
	 */
	protected $user;

	public function setUser(\Application\Entity\User $user){
		$this->user = $user;
	}

	public function getUser(){
		return $this->user;
	}



	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getRazred()
	{
		return $this->razred;
	}

	/**
	 * @param int $razred
	 */
	public function setRazred($razred)
	{
		$this->razred = $razred;
	}

	/**
	 * @return string
	 */
	public function getOdjel()
	{
		return $this->odjel;
	}

	/**
	 * @param string $odjel
	 */
	public function setOdjel($odjel)
	{
		$this->odjel = $odjel;
	}


	/**
	 * Set podrucniOdjel
	 *
	 * @param \Application\Entity\PodrucniOdjel $podrucniOdjel
	 * @return \Application\Entity\Razred
	 */
	public function setPodrucniOdjel(\Application\Entity\PodrucniOdjel $podrucniOdjel = null)
	{
		$this->podrucniOdjel = $podrucniOdjel;

		return $this;
	}


	/**
	 * Get podrucniOdjel
	 *
	 * @return \Application\Entity\PodrucniOdjel
	 */
	public function getPodrucniOdjel()
	{
		return $this->podrucniOdjel;
	}


	/**
	 * Remove podrucniOdjel
	 *
	 * @param podrucniOdjel $podrucniOdjel
	 */
	public function removePodrucniOdjel(\Application\Entity\PodrucniOdjel $podrucniOdjel)
	{
		$this->podrucniOdjel->removeElement($podrucniOdjel);
	}







	/**
	 * Add ucenici
	 *
	 * @param \Application\Entity\Clan $ucenici
	 * @return \Application\Entity\Razred
	 */
	public function addUcenici(Collection $ucenici)
	{
		foreach ($ucenici as $ucenik) {
			$ucenik->addRazred($this);
			$this->ucenici->add($ucenik);
		}
	}

	/**
	 * Remove ucenici
	 *
	 * @param \Application\Entity\Clan $ucenici
	 */
	public function removeUcenici(Collection $ucenici)
	{
		foreach ($ucenici as $ucenik) {
			$ucenik->addRazred(null);
			$this->ucenici->removeElement($ucenik);
		}
	}

	/**
	 * Get ucenici
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getUcenici()
	{
		return $this->ucenici;
	}


	public function prebaciUVisiRazred (){
		$this->razred += 1;
	}



}