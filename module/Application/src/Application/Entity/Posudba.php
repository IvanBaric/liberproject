<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Application\Repository\PosudbaRepository")
 * @ORM\Table(name="knjiznica_posudba", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Posudba {

    public function __construct(){

    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @var datetime
     *
     * @ORM\Column(name="createdAt", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdAt;


    /**
     * @var datetime
     *
     * @ORM\Column(name="updatedAt", type="datetime", precision=0, scale=0, nullable=true, unique=false, options={"default" = NULL})
     */
    private $updatedAt;



    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\KataloskiBroj", inversedBy="posudbe", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kataloski_broj_id", referencedColumnName="id")})
     */
    private $kataloskiBroj;




    /**
     * @var \Doctrine\Common\Collections\\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Clan", inversedBy="posudbe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clan_id", referencedColumnName="id")})
     */
    private $clan;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\Application\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



        /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return \Application\Entity\Autor
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        if($this->createdAt instanceof \DateTime){
            $date = $this->createdAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return \Application\Entity\Autor
     */
    public function setUpdatedAt(){
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(){
        if($this->updatedAt instanceof \DateTime){
            $date = $this->updatedAt;
            $result = $date->format('Y-m-d H:i:s');

            if ($result) {
                return $result;
            } else { // format failed
                return "Unknown Time";
            }
        }
    }




    	/**
    	 * Set clan
    	 *
    	 * @param \Application\Entity\Clan $clan
    	 * @return \Application\Entity\Posudba
    	 */
    	public function setClan(\Application\Entity\Clan $clan = null)
    	{
    		$this->clan = $clan;

    		return $this;
    	}

    	/**
    	 * Get clan
    	 *
    	 * @return \Application\Entity\Clan
    	 */
    	public function getClan()
    	{
    		return $this->clan;
    	}

        /**
    	 * Remove clan
    	 *
    	 * @param \clan $clan
    	 */
        public function removeClan(\Application\Entity\Clan $clan)
    	{
    		$this->clan->removeElement($clan);
    	}




    /**
     * Set kataloskiBroj
     *
     * @param \Application\Entity\KataloskiBroj $kataloskiBroj
     * @return \Application\Entity\Posudba
     */
    public function setKataloskiBroj(\Application\Entity\KataloskiBroj $kataloskiBroj = null)
    {
        $this->kataloskiBroj = $kataloskiBroj;

        return $this;
    }

    /**
     * Get kataloskiBroj
     *
     * @return \Application\Entity\KataloskiBroj
     */
    public function getKataloskiBroj()
    {
        return $this->kataloskiBroj;
    }

    /**
     * Remove kataloskiBroj
     *
     * @param \kataloskiBroj $kataloskiBroj
     */
    public function removeKataloskiBroj(\Application\Entity\KataloskiBroj $kataloskiBroj)
    {
        $this->kataloskiBroj->removeElement($kataloskiBroj);
    }


    public function produljiPosudbu(){
        $this->createdAt = new \DateTime();
    }




}