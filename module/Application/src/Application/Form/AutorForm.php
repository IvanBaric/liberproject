<?php
namespace Application\Form;
use Application\Entity\Razred;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class AutorForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{


		parent::__construct('AutorForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new DoctrineHydrator($objectManager));


		$this->add(array(
				'name' => 'ime',
				'attributes' => array(
						'type' => 'text',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'ime',
						'label' => 'Ime',
				),
		));


		$this->add(array(
				'name' => 'prezime',
				'attributes' => array(
						'type' => 'text',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'prezime',
						'label' => 'Prezime',
				),
		));







		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));


		#$this->add(new \MyModule\Form\CommonFieldset());

	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
			$inputFilter->add ( $factory->createInput(
					array (
						'name' => 'ime',
						'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
						'validators' => array()
					)

				)
			);


			$inputFilter->add ( $factory->createInput(array (
				'name' => 'prezime',
				'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
				'validators' => array()
			)));



			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}