<?php
namespace Application\Form;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class DjelatnikForm extends Form {

	public function __construct(ObjectManager $objectManager)
	{
		parent::__construct('clanForm');
		#$this->setAttribute('action', '/signup');
		$this->setAttribute('method', 'post');
		#$this->setAttribute('enctype', 'multipart/form-data');

		$this->setHydrator(new DoctrineHydrator($objectManager));



		$djelatnikFieldset = new \Application\Form\Fieldset\DjelatnikFieldset($objectManager);
		$djelatnikFieldset->setUseAsBaseFieldset(true);
		$this->add($djelatnikFieldset);


		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

		#$this->add(new \MyModule\Form\CommonFieldset());

	}



}