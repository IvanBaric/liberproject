<?php
namespace Application\Form\Fieldset;

use Application\Entity\Djelatnik;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class DjelatnikFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('clan');

        $hydrator = new DoctrineHydrator($objectManager);

        $this->setHydrator($hydrator)->setObject(new Djelatnik());

        

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'ime',
            'options' => array(
                'label' => 'Ime*'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'prezime',
            'options' => array(
                'label' => 'Prezime*'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Select',
            'name'    => 'spol',
            'attributes' => array(
                'class' => 'selectpicker',
            ),
            'options' => array(
                'label' => 'Spol',
                'empty_option' => 'Odaberite spol',
                'value_options' => array('M' => 'M', 'Z'=> 'Ž'),
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'kontakt',
            'options' => array(
                'label' => 'Kontakt'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'kontakt2',
            'options' => array(
                'label' => 'Kontakt2'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Email',
            'name'    => 'email',
            'options' => array(
                'label' => 'Email'
            )
        ));



    }

    public function getInputFilterSpecification()
    {
        return array(
            'ime' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'prezime' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'spol' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'kontakt' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'kontakt2' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'email' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),


        );
    }
}