<?php
namespace Application\Form\Fieldset;

use Application\Entity\Razred;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class RazredFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('razred');


        $this->setHydrator(new DoctrineHydrator($objectManager, true))->setObject(new Razred());

        #$popisRazreda = $objectManager->getRepository('Application\Entity\Razred')->getDropdown();
        $popisPodrucnihOdjela = $objectManager->getRepository('Application\Entity\PodrucniOdjel')->getDropdown();
        




//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($razredi); die();

//        $this->add(array(
//            'type'    => 'Zend\Form\Element\Text',
//            'name'    => 'id',
//            'options' => array(
//                'label' => 'Id'
//            )
//        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'razred',
            'attributes' => array(
                'class' => '',
            ),
            'options' => array(
                'label' => 'Razred',
                'empty_option' => 'Odaberite jedan od ponuđenih razreda',
                'value_options' => array(
                    '1' => "1.",
                    '2' => "2.",
                    '3' => "3.",
                    '4' => "4.",
                    '5' => "5.",
                    '6' => "6.",
                    '7' => "7.",
                    '8' => "8."
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'odjel',
            'attributes' => array(
                'class' => '',
            ),
            'options' => array(
                'label' => 'Odjel',
                'empty_option' => 'Odaberite jedan od ponuđenih odjela',
                'value_options' => array(
                    'a' => 'a',
                    'b' => 'b',
                    'c' => 'c'
                ),
            )
        ));



        if(count($popisPodrucnihOdjela)>0) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'podrucniOdjel',
                'attributes' => array(
                    'class' => '',
                ),
                'options' => array(
                    'label' => 'Područni odjel',
                    'empty_option' => 'Odaberite jedan od ponuđenih područnih odjela',
                    'value_options' => $popisPodrucnihOdjela
                )
            ));
        }



//        $this->add(array(
//            'type'    => 'Zend\Form\Element\Text',
//            'name'    => 'razred',
//            'options' => array(
//                'label' => 'Razred'
//            )
//        ));



    }

    public function getInputFilterSpecification()
    {
        return array(
//            'id' => array(
//                'required' => false
//            ),

            'razred' => array(
                'required' => true
            ),

            'odjel' => array(
                'required' => false
            ),


            'podrucniOdjel' => array(
                'required' => false
            ),

        );
    }
}