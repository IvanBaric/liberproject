<?php
namespace Application\Form\Fieldset;

use Application\Entity\PodrucniOdjel;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class PodrucniOdjelFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('podrucniOdjel');

        $this->setHydrator(new DoctrineHydrator($objectManager),false)->setObject(new PodrucniOdjel());


        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'naziv',
            'options' => array(
                'label' => 'Naziv područnog odjela'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'godineSkolovanja',
            'attributes' => array(

            ),
            'options' => array(
                'label' => 'Godine školovanja u područnome odjelu',
                'empty_option' => 'Odaberite godine školovanja u područnome odjelu',
                'value_options' => array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8
                ),
            )
        ));

    }

    public function getInputFilterSpecification()
    {
        return array(
            'naziv' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
                'validators' => array(

                ),
            ),

            'godineSkolovanja' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),
        );
    }
}