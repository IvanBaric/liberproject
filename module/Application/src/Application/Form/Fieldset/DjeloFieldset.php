<?php
namespace Application\Form\Fieldset;

use Application\Entity\Djelo;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class DjeloFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('djelo');

        $hydrator = new DoctrineHydrator($objectManager);

        $this->setHydrator($hydrator)->setObject(new Djelo());

        

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'naziv',
            'attributes' => array(
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Naziv djela'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Number',
            'name'    => 'godinaIzdanja',
            'attributes' => array(
                'class' => ''
            ),
            'options' => array(
                'label' => 'Godina izdanja'
            )
        ));





        $popisIzdavaca = $objectManager->getRepository('Application\Entity\Izdavac')->getDropdown();

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'izdavac',
            'attributes' => array(
                'class' => '',
            ),
            'options' => array(
                'label' => 'Izdavač',
                'empty_option' => 'Odaberite jednog od ponuđenih izdavača',
                'value_options' => $popisIzdavaca
            )
        ));


        $popisMedija = $objectManager->getRepository('Application\Entity\Medij')->getDropdown();

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'medij',
            'attributes' => array(
                'class' => '',
            ),
            'options' => array(
                'label' => 'Popis medija',
                'empty_option' => 'Odaberite jednog od ponuđenih medija',
                'value_options' => $popisMedija
            )
        ));

        $popisKnjizevnihVrsta = $objectManager->getRepository('Application\Entity\KnjizevnaVrsta')->getDropdown();

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'knjizevnaVrsta',

            'attributes' => array(
                'class' => '',
            ),
            'options' => array(
                'label' => 'Popis književnih vrsta',

                'empty_option' => 'Odaberite jednog od ponuđenih književnih vrsta',
                'value_options' => $popisKnjizevnihVrsta
            )
        ));



        $autorFieldset = new AutorFieldset($objectManager);
        $this->add(array(
            'type'    => 'Zend\Form\Element\Collection',
            'name'    => 'autori',
            'options' => array(
                'count'           => 1,
                'target_element' => $autorFieldset,
                'should_create_template' => true,
                'template_placeholder' => '__index__',
                'allow_add' => true,


            )
        ));



//        $razredFieldset = new \Application\Form\RazredFieldset($objectManager);
//        $this->add($razredFieldset);



    }

    public function getInputFilterSpecification()
    {
        return array(
            'naziv' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'izdavac' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'medij' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'knjizevnaVrsta' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),




        );
    }
}