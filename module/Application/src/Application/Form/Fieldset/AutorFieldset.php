<?php
namespace Application\Form\Fieldset;

use Application\Entity\Autor;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AutorFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('autori');

        $hydrator = new DoctrineHydrator($objectManager);

        $this->setHydrator($hydrator)->setObject(new Autor());

        $popisAutora = $objectManager->getRepository('Application\Entity\Autor')->getDropdown();

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id',
            'attributes' => array(
                'class' => '',

            ),
            'options' => array(
                'id' => 'id',
                'label' => 'Autor',
                'empty_option' => 'Odaberite jednog od ponuđenih autora',
                'value_options' => $popisAutora
            )
        ));

    }

    public function getInputFilterSpecification()
    {
        return array(
            'id' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

        );
    }
}