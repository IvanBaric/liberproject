<?php
namespace Application\Form\Fieldset;

use Application\Entity\Ucenik;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class UcenikFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct('clan');

        $hydrator = new DoctrineHydrator($objectManager);

        $this->setHydrator($hydrator)->setObject(new Ucenik());

        

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'ime',
            'options' => array(
                'label' => 'Ime*'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'prezime',
            'options' => array(
                'label' => 'Prezime*'
            )
        ));


        $popisRazreda = $objectManager->getRepository('Application\Entity\Razred')->getDropdown();

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'razred',
            'attributes' => array(
                'class' => 'selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'label' => 'Razred*',
                'empty_option' => 'Odaberite jedan od ponuđenih razreda',
                'value_options' => $popisRazreda
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Select',
            'name'    => 'spol',
            'attributes' => array(
                'class' => 'selectpicker',
            ),
            'options' => array(
                'label' => 'Spol',
                'empty_option' => 'Odaberite spol',
                'value_options' => array('M' => 'M', 'Z'=> 'Ž'),
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'kontakt',
            'options' => array(
                'label' => 'Kontakt'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Text',
            'name'    => 'kontakt2',
            'options' => array(
                'label' => 'Kontakt2'
            )
        ));

        $this->add(array(
            'type'    => 'Zend\Form\Element\Email',
            'name'    => 'email',
            'options' => array(
                'label' => 'Email'
            )
        ));


//        $razredFieldset = new \Application\Form\RazredFieldset($objectManager);
//        $this->add($razredFieldset);



    }

    public function getInputFilterSpecification()
    {
        return array(
            'ime' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'prezime' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'razred' => array(
                'required' => true,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'spol' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'kontakt' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'kontakt2' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),

            'email' => array(
                'required' => false,
                'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
            ),


        );
    }
}