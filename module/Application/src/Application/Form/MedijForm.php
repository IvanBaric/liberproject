<?php
namespace Application\Form;
use Application\Entity\Razred;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class MedijForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{


		parent::__construct('AutorForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new DoctrineHydrator($objectManager));


		$this->add(array(
				'name' => 'naziv',
				'attributes' => array(
						'type' => 'text',
						'class' => 'form-control',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'naziv',
						'label' => 'Naziv',
						'label_attributes' => array(
								'class' => 'control-label'
						),

				),
		));

		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));


		#$this->add(new \MyModule\Form\CommonFieldset());
	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
			$inputFilter->add ( $factory->createInput(
					array (
						'name' => 'naziv',
						'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
						'validators' => array()
					)

				)
			);





			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}