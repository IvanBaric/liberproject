<?php
namespace Application\Form;
use Application\Entity\Razred;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class IzdavacForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{


		parent::__construct('IzdavacForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new DoctrineHydrator($objectManager));


		$this->add(array(
				'type'    => 'Zend\Form\Element\Text',
				'name'    => 'naziv',
				'options' => array(
					'id' => 'naziv',
					'label' => 'Naziv izdavačke kuće*'
				)
		));

		$this->add(array(
				'type'    => 'Zend\Form\Element\Text',
				'name'    => 'adresa',
				'options' => array(
					'id' => 'adresa',
					'label' => 'Adresa'
				)
		));

		$this->add(array(
				'type'    => 'Zend\Form\Element\Text',
				'name'    => 'mjesto',
				'options' => array(
						'id' => 'mjesto',
						'label' => 'Mjesto'
				)
		));

		$this->add(array(
				'type'    => 'Zend\Form\Element\Text',
				'name'    => 'telefon',
				'options' => array(
						'id' => 'telefon',
						'label' => 'Telefon'
				)
		));

		$this->add(array(
				'type'    => 'Zend\Form\Element\Email',
				'name'    => 'email',
				'options' => array(
						'id' => 'email',
						'label' => 'Email'
				)
		));


		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));


		#$this->add(new \MyModule\Form\CommonFieldset());

	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
			$inputFilter->add ( $factory->createInput(
					array (
						'name' => 'naziv',
						'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
						'validators' => array()
					),

					array (
							'name' => 'adresa',
							'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
							'validators' => array()
					),

					array (
							'name' => 'mjesto',
							'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
							'validators' => array()
					),

					array (
							'name' => 'telefon',
							'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
							'validators' => array()
					),

					array (
							'name' => 'email',
							'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
							'validators' => array()
					)
				)
			);





			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}