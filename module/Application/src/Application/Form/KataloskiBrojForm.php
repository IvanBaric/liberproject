<?php
namespace Application\Form;
use Application\Entity\Razred;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class KataloskiBrojForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{


		parent::__construct('KataloskiBrojForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new DoctrineHydrator($objectManager));


		$this->add(array(
				'name' => 'kataloskiBroj',
				'attributes' => array(
						'type' => 'text',
						'class' => 'tags input-lg',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'kataloskiBroj',
						'label' => 'Kataloški broj',

						'label_attributes' => array(
								'class' => ''
						),

				),
		));




		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

		#$this->add(new \MyModule\Form\CommonFieldset());

	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
			$inputFilter->add ( $factory->createInput(
					array (
						'name' => 'kataloskiBroj',
						'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
						'validators' => array()
					)

				)
			);



			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}