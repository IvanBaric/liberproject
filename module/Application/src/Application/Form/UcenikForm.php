<?php
namespace Application\Form;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class UcenikForm extends Form {

	public function __construct(ObjectManager $objectManager)
	{
		parent::__construct('clanForm');
		#$this->setAttribute('action', '/signup');
		$this->setAttribute('method', 'post');
		#$this->setAttribute('enctype', 'multipart/form-data');

		$this->setHydrator(new DoctrineHydrator($objectManager));



		$ucenikFieldset = new \Application\Form\Fieldset\UcenikFieldset($objectManager);
		$ucenikFieldset->setUseAsBaseFieldset(true);
		$this->add($ucenikFieldset);


		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

		#$this->add(new \MyModule\Form\CommonFieldset());
	}



}