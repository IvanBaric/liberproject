<?php
namespace Application\Form;
use Application\Entity\Razred;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

class PosudbaForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{


		parent::__construct('PosudbaForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new DoctrineHydrator($objectManager));


		$this->add(array(
				'name' => 'clan',
				'attributes' => array(
						'type' => 'text',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'clan',
						'label' => 'Član',
				),
		));


		$this->add(array(
				'name' => 'kataloskiBroj',
				'attributes' => array(
						'type' => 'text',
						'placeholder' => ''
				),
				'options' => array(
						'id' => 'kataloskiBroj',
						'label' => 'Kataloški broj',

				),
		));

		$this->add(array(
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'produljiPosudbu',
				'options' => array(
						'label' => 'Produlji posudbu',
//						'use_hidden_element' => true,
//						'checked_value' => 'da',
						'unchecked_value' => 'ne'
				),
				'attributes' => array(
						'value' => 'da'
				)
		));



		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

		#$this->add(new \MyModule\Form\CommonFieldset());
	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
			$inputFilter->add ( $factory->createInput(
					array (
						'name' => 'clan',
						'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
						'validators' => array()
					)

				)
			);


			$inputFilter->add ( $factory->createInput(array (
				'name' => 'kataloskiBroj',
				'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
				'validators' => array()
			)));

			$inputFilter->add ( $factory->createInput(array (
					'name' => 'produljiPosudbu',
					'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
					'validators' => array()
			)));



			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}