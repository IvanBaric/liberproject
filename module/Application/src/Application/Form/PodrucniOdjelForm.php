<?php
namespace Application\Form;
use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;
use Application\Form\Fieldset\PodrucniOdjelFieldset;

class PodrucniOdjelForm extends Form {

	public function __construct(ObjectManager $objectManager)
	{
		parent::__construct('podrucniOdjelForm');
		#$this->setAttribute('action', '/signup');
		$this->setAttribute('method', 'post');
		#$this->setAttribute('enctype', 'multipart/form-data');

		$this->setHydrator(new DoctrineHydrator($objectManager));


		$podrucniOdjelFieldset = new PodrucniOdjelFieldset($objectManager);
		$podrucniOdjelFieldset->setUseAsBaseFieldset(true);
		$this->add($podrucniOdjelFieldset);


		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

		#$this->add(new \MyModule\Form\CommonFieldset());
	}
}