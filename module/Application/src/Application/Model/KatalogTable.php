<?php
namespace Application\Model;

use Doctrine\Common\Persistence\ObjectManager;

class KatalogTable extends \MyModule\Model\MyModel
{
        protected $_em;

        public function __construct(ObjectManager $objectManager){
            $this->_em = $objectManager;
        }



        public function getArrayFromFormData($formData){
            $explode = explode(',', $formData['kataloskiBroj']);
            return $kataloskiBrojevi = array_unique($explode);
        }


        public function fetchKataloskiBrojevi($sviKataloskiBrojevi){
            if(is_array($sviKataloskiBrojevi)){

                $sviKataloskiBrojeviArray = array();

                foreach($sviKataloskiBrojevi as $kBroj){
                    foreach($kBroj as $value){
                        array_push($sviKataloskiBrojeviArray, $value);
                    }
                }

                return $sviKataloskiBrojeviArray;
            }

            return false;

        }


    public function save($arrayDiff, $entity){
        if($arrayDiff){
            $saved = array();

            foreach($arrayDiff as $broj){
                $kataloskiBroj = new \Application\Entity\KataloskiBroj();
                $kataloskiBroj->setKataloskiBroj($broj);
                $kataloskiBroj->setKatalog($entity);
                $this->_em->persist($kataloskiBroj);

                array_push($saved, $kataloskiBroj);
            }

            $this->_em->flush();
            return $saved;
        }
        return false;
    }


    public function countKataloskiBrojevi($kataloskiBrojeviDjela){
        $count = array();

        foreach($kataloskiBrojeviDjela as $kbr){
            if(array_key_exists('idKataloskiBroj', $kbr)){
                if(!empty($kbr['idKataloskiBroj'])){
                    array_push($count, $kbr['idKataloskiBroj']);
                }
            }
        }

        return count($count);
    }

}