<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PodrucniOdjelRepository extends EntityRepository {


    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\PodrucniOdjel', 'po')
            ->select('po.id as id, po.naziv as naziv, po.godineSkolovanja as godineSkolovanja, COUNT(raz.id) as brojRazreda, COUNT(uc.id) as brojUcenika')
            ->leftJoin('po.razredi', 'raz')
            ->leftJoin('raz.ucenici', 'uc')
            ->groupBy('po.id')
            ->orderBy($params['order_by'], $params['order']);




        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('po.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));

        }


        $dql = $qb->getQuery()->getDQL();

        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }


    public function hasOrphans($id){
        $rezultat = $this->getBrojRazredaPodrucnogOdjela($id);

        return ($rezultat['brojRazreda']>0) ? true : false;
    }

    public function getDropdown(){
        $results = $this->getEntityManager()->getRepository('Application\Entity\PodrucniOdjel')->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $array = array();
        foreach($results as $result){
            $id = $result->getId();
            $podrucniOdjel = $result->getNaziv();


            $array[$id] = $podrucniOdjel;
        }

        return $array;
    }


    public function getBrojRazredaPodrucnogOdjela($id){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\PodrucniOdjel', 'po')
            ->select('COUNT(raz.id) as brojRazreda')
            ->leftJoin('po.razredi', 'raz')
            ->where('po.id = :poId')->setParameter(':poId', $id)
            ->groupBy('po.id');



        return $qb->getQuery()->getOneOrNullResult();
    }




}