<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AutorRepository extends EntityRepository {

    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Autor', 'a')
            ->select('a.id as id, CONCAT( CONCAT(a.ime, \' \'),  a.prezime) as ime, a.prezime as prezime')
            ->orderBy($params['order_by'], $params['order']);

        if(!empty($params['search'])) {
            $qb->orwhere($qb->expr()->like('CONCAT( CONCAT(a.ime, \' \'),  a.prezime)', $qb->expr()->literal('%' . $params['search'] . '%')));
        }



        $dql = $qb->getQuery()->getDQL();

        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }


    public function getDropdown(){
        $results = $this->getEntityManager()->getRepository('Application\Entity\Autor')->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $array = array();
        foreach($results as $result){
            $id = $result->getId();
            $naziv = $result->getIme(). " ". $result->getPrezime();


            $array[$id] = $naziv;
        }

        return $array;
    }
    
    
    
    public function getLastInserted($num=5){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Autor', 'a')
            ->select('a')
            ->orderBy('a.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }
    
    public function hasOrphans($id){
        $entity = $this->getEntityManager()->getRepository('Application\Entity\Autor')->find($id);
        if(count($entity->getDjela())>0){
            return true;
        }
        return false;
    }

    public function recordExists($entity){
        return $recordExists = $this->getEntityManager()->getRepository('Application\Entity\Autor')->findOneBy(array('ime' => $entity->getIme(), 'prezime'=> $entity->getPrezime()));
    }

}