<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class RazredRepository extends EntityRepository {

    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Razred', 'raz')
            ->select('raz.id as id, CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel) as razred,  po.naziv as podrucniOdjel, COUNT(uc.id) as brojUcenika')
            ->leftJoin('raz.podrucniOdjel', 'po')
            ->leftJoin('raz.ucenici', 'uc')
            ->groupBy('raz.id')
            ->orderBy($params['order_by'], $params['order']);

        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel)', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orwhere($qb->expr()->like('po.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
        }



        $dql = $qb->getQuery()->getDQL();


        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }


    public function hasOrphans($id){
        $brojUcenikaRazreda = $this->getBrojUcenikaRazreda($id);

        return ($brojUcenikaRazreda['brojUcenika']>0) ? true : false;
    }


    public function recordExists($entity){
        $odjel = ($entity->getOdjel()) ? $entity->getOdjel() : false;
        $podrucniOdjel = ($entity->getPodrucniOdjel()) ? $entity->getPodrucniOdjel()->getId() : false;

        $condition = array('razred' => $entity->getRazred());
        ($odjel) ? $condition['odjel'] = $odjel : null;
        ($podrucniOdjel) ? $condition['podrucniOdjel'] = $podrucniOdjel : null;

        return $recordExists = $this->getEntityManager()->getRepository('Application\Entity\Razred')->findOneBy($condition);
    }

    public function getDropdown(){
        $razredi = $this->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $popisRazreda = array();
        foreach($razredi as $raz){
            $id = $raz->getId();
            $razred = $raz->getRazred();
            $odjel = !empty($raz->getOdjel()) ? $raz->getOdjel() : "";

            $popisRazreda[$id] = $razred.".".$odjel;
        }

        return $popisRazreda;
    }



    public function getBrojUcenikaRazreda($id){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Razred', 'raz')
            ->select('COUNT(uc.id) as brojUcenika ')
            ->leftJoin('raz.ucenici', 'uc')
            ->where('raz.id = :razredId')->setParameter(':razredId', $id)
            ->groupBy('raz.id');

        return $qb->getQuery()->getOneOrNullResult();
    }




    //TODO: napisati upit za upis učenika u viši razred škole

}