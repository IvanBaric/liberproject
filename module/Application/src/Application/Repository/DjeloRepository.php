<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DjeloRepository extends EntityRepository {

    public function fetchAll($params){

        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Djelo', 'dj');
        $qb->select('dj, i, kv, m, a, k');
        $qb->join('dj.izdavac', 'i');
        $qb->leftJoin('dj.katalozi', 'k');
        $qb->join('dj.knjizevnaVrsta', 'kv');
        $qb->join('dj.medij', 'm');
        $qb->join('dj.autori', 'a');
        $qb->orderBy($params['order_by'], $params['order']);
//        $qb->where('dj.isVisible is NULL');


//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($qb->getQuery()->getResult()); die();

        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('dj.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('dj.godinaIzdanja', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('i.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('m.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('kv.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
        }


        $dql = $qb->getQuery()->getDQL();

        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;


    }

    public function hasOrphans($id){
        $entity = $this->getEntityManager()->getRepository('Application\Entity\Katalog')->findOneBy(array('djelo' => $id));
        if(count($entity)>0){
            return true;
        }
        return false;
    }
}