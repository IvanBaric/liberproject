<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;

class KataloskiBrojRepository extends EntityRepository {
        public function fetchAll(){
            $qb = $this->_em->createQueryBuilder();

            $qb->from('Application\Entity\KataloskiBroj', 'k')
                ->select('k.kataloskiBroj');
                //->where('k.kataloskiBroj IN (:name)')->setParameter('name', $name);

            return $qb->getQuery()->getResult();
        }

    public function hasOrphans($id){
        $posudbe = $this->isKataloskiBrojPosudjen($id);

        return (count($posudbe)>0) ? true : false;
    }


    public function isKataloskiBrojPosudjen($id){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Katalog', 'k')
            ->select('p.id as posudbaId')
            ->leftJoin('k.kataloskiBrojevi', 'kbr')
            ->leftJoin('kbr.posudbe', 'p')

            ->where('p.updatedAt IS NULL')
            ->andWhere('p.kataloskiBroj = :kataloskiBroj')->setParameter(':kataloskiBroj', $id);


        return $qb->getQuery()->getResult();
    }
}