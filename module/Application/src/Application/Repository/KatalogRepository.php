<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class KatalogRepository extends EntityRepository {


    public function fetchAll($params){

        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Katalog', 'k')
            ->select('k.id as id, dj.id as idDjelo, dj.naziv as naziv, dj.godinaIzdanja as godinaIzdanja, i.naziv as izdavac, kv.naziv as knjizevnaVrsta, m.naziv as medij, COUNT(kbr.kataloskiBroj) as brojPrimjeraka, COUNT(p.kataloskiBroj) as posudjeniPrimjerci ,COUNT(kbr.kataloskiBroj) - COUNT(p.kataloskiBroj) as dostupnostPrimjeraka')
            ->join('k.djelo', 'dj')
            ->leftJoin('k.kataloskiBrojevi', 'kbr')
            ->leftJoin('kbr.posudbe', 'p')
            ->groupBy('dj.id');
        $qb->join('dj.izdavac', 'i');
        $qb->join('dj.knjizevnaVrsta', 'kv');
        $qb->join('dj.medij', 'm');
        $qb->orderBy($params['order_by'], $params['order']);


        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('dj.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('dj.godinaIzdanja', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('i.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('m.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('kv.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
        }

        $qb->andWhere('p.updatedAt IS NULL');

       # $qb->andWhere('uc.user ='. $this->getUserId());

        $dql = $qb->getQuery()->getDQL();

        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;


//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($qb->getQuery()->getResult()); die();

    }






    public function fetchAllSSS($params){

//TODO: mislim da mi za ovaj upit ne treba queryBuilder (zbog autora koje mi vraća kao array)
//TODO: možda i treba zbog paginacije i drugih uvjeta
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Katalog', 'k');
        $qb->select('k.id as id, dj.id as idDjelo, dj.naziv as naziv, dj.godinaIzdanja as godinaIzdanja, i.naziv as izdavac, kv.naziv as knjizevnaVrsta, m.naziv as medij');
        $qb->join('k.djelo', 'dj');
        $qb->join('dj.izdavac', 'i');
        $qb->join('dj.knjizevnaVrsta', 'kv');
        $qb->join('dj.medij', 'm');
        $qb->orderBy($params['order_by'], $params['order']);
        #$qb->join('dj.autori', 'a');


        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('dj.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('dj.godinaIzdanja', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('i.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('m.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('kv.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
        }

//
//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($qb->getQuery()->getResult()); die();


        
        $dql = $qb->getQuery()->getDQL();

        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }



    public function hasOrphans($id){
        $result = $this->hasKataloskiBroj($id);
        


        return (count($result['kataloskiBrojevi'])>0) ? true : false;
    }

    public function getKataloskiBrojeviDjela($id){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Katalog', 'k')
            ->select('k.id as idKatalog, p.id as posudbaId, dj.naziv, CONCAT( CONCAT(c.ime, \' \'),  c.prezime) as ime, CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel) as razred, po.naziv as podrucniOdjel, kbr.kataloskiBroj, p.createdAt as datumPosudbe, DATE_DIFF(CURRENT_TIME(), p.createdAt) as trajanjePosudbe, kbr.id as idKataloskiBroj')
            ->join('k.djelo', 'dj')
            ->leftJoin('k.kataloskiBrojevi', 'kbr')
            ->leftJoin('kbr.posudbe', 'p')
            ->leftJoin('p.clan', 'c')
            ->leftJoin('c.razred', 'raz')
            ->leftJoin('raz.podrucniOdjel', 'po')
            ->where('p.updatedAt IS NULL')
            ->andWhere('dj.id = :id')->setParameter(':id', $id);


        return $qb->getQuery()->getResult();
    }



    public function hasKataloskiBroj($id){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Katalog', 'k')
            ->select('COUNT(kbr.id) as kataloskiBrojevi')
            ->leftJoin('k.kataloskiBrojevi', 'kbr')
            ->groupBy('kbr.katalog')
            ->where('kbr.katalog = :kataloskiBroj')->setParameter(':kataloskiBroj', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }



    public function getAllIds(){
        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Katalog', 'k')
            ->select('k');

        return $qb->getQuery()->getResult();
    }




}