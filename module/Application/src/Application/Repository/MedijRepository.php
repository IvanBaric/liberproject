<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class MedijRepository extends EntityRepository {

    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Medij', 'm')
            ->select('m.id as id, m.naziv as naziv')
            ->orderBy($params['order_by'], $params['order']);

        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('m.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
        }

        $dql = $qb->getQuery()->getDQL();


        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

    public function hasOrphans($id){
        $entity = $this->getEntityManager()->getRepository('Application\Entity\Djelo')->findOneBy(array('medij' => $id));
        if(count($entity)>0){
            return true;
        }
        return false;
    }

    public function recordExists($entity){
        return $recordExists = $this->getEntityManager()->getRepository('Application\Entity\Medij')->findOneBy(array('naziv' => $entity->getNaziv()));
    }


    public function getDropdown(){
        $results = $this->getEntityManager()->getRepository('Application\Entity\Medij')->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $array = array();
        foreach($results as $result){
            $id = $result->getId();
            $naziv = $result->getNaziv();


            $array[$id] = $naziv;
        }

        return $array;
    }


    public function getLastInserted($num=5){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Medij', 'm')
            ->select('m')
            ->orderBy('m.createdAt', 'DESC');

        return $qb->getQuery()->getResult();
    }


}