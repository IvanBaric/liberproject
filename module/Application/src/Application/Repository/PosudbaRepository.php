<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
class PosudbaRepository extends EntityRepository {

    //testni array --> clanId ili razredId
    protected $array = array(
        'clanId' => 4,
//        'razredId' => 13
    );

    protected $tipPosudbe = array(
//        'aktivnePosudbe' => true,
        'vracenePosudbe' => true
    );



    public function fetchAll($params){
    $qb = $this->_em->createQueryBuilder();

    $qb->from('Application\Entity\Posudba', 'p')
        ->select('p.id as id, p.createdAt as zapocetaPosudba, p.updatedAt as zavrsenaPosudba, dj.naziv as nazivDjela, CONCAT( CONCAT(c.ime, \' \'),  c.prezime) as ime, CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel) as razred, po.naziv as podrucniOdjel, DATE_DIFF(CURRENT_TIME(), p.createdAt) as trajanjePosudbe')
        ->join('p.kataloskiBroj', 'kbr')
        ->join('kbr.katalog', 'kat')
        ->join('kat.djelo', 'dj')
        ->join('p.clan', 'c')
        ->join('c.razred', 'raz')
        ->leftJoin('raz.podrucniOdjel', 'po')
        ->orderBy($params['order_by'], $params['order']);


        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('dj.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('po.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orwhere($qb->expr()->like('CONCAT( CONCAT(c.ime, \' \'),  c.prezime)', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel)', $qb->expr()->literal('%' . $params['search'] . '%')));

            #$qb->orWhere('DATE_DIFF(CURRENT_TIME(), p.createdAt) = :num')->setParameter(':num', $params['search']);

            if (strpos($params['search'], 'dan') !== false || strpos($params['search'], 'dana') !== false) {
                $explode = explode(' ',$params['search']);
                if(count($explode) == 2){
                    $num = $explode[0];
                    if(is_numeric($num)){
                         $qb->where($qb->expr()->gt('DATE_DIFF(CURRENT_TIME(), p.createdAt)', $num));
                    }
                }
            }

        }

        $qb->andWhere('p.updatedAt IS NULL');



        $dql = $qb->getQuery()->getDQL();


        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }







    //TODO: napisati upit za popis "aktivnih posudbi" pojedinog učenika (vraća broj ili naziv posuđenih knjiga + trajanje, ovisno o parametrima)
    // array - ucenikId ili razredId
    public function getPosudbe($tipPosudbe = array(), $array=null){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('Application\Entity\Posudba', 'p')
            ->select('p.id, p.createdAt, p.updatedAt, dj.naziv, c.ime, c.prezime, raz.razred, raz.odjel, po.naziv as podrucniOdjel, DATE_DIFF(CURRENT_TIME(), p.createdAt) as trajanjePosudbe')
            ->join('p.kataloskiBroj', 'kbr')
            ->join('kbr.katalog', 'kat')
            ->join('kat.djelo', 'dj')
            ->join('p.clan', 'c')
            ->join('c.razred', 'raz')
            ->leftJoin('raz.podrucniOdjel', 'po');

        if(array_key_exists('aktivnePosudbe', $this->tipPosudbe)) {
            $qb->where('p.updatedAt IS NULL');
        }

        if(array_key_exists('vracenePosudbe', $this->tipPosudbe)) {
            $qb->where('p.updatedAt IS NOT NULL');
        }



        if(array_key_exists('clanId', $this->array)){
            $qb->andWhere('c.id = :clanId')->setParameter(':clanId', $this->array['clanId']);
        }

        if(array_key_exists('razredId', $this->array)){
            $qb->andWhere('raz.id = :razredId')->setParameter(':razredId', $this->array['razredId']);
        }

        return $qb->getQuery()->getResult();
    }


    //TODO: prosječna posudba


}