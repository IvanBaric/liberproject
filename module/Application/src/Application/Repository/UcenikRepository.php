<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;



// http://kristopherwilson.com/2013/02/26/using-doctrine-paginator-with-zend-framework-2-paginator/
class UcenikRepository extends EntityRepository {
    


    
    public function fetchAll($params){
        

        
        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Ucenik', 'uc')
            ->select('uc.id as id, CONCAT( CONCAT(uc.ime, \' \'),  uc.prezime) as ime, uc.prezime as prezime, CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel) as razred, po.naziv as podrucniOdjel, COUNT(p.id) as aktivnePosudbe ')
            ->join('uc.razred', 'raz')
            ->leftJoin('raz.podrucniOdjel', 'po')
            ->leftJoin('uc.posudbe', 'p', 'WITH', 'p.updatedAt IS NULL')
            ->groupBy('uc.id')
            ->orderBy($params['order_by'], $params['order']);

            if(!empty($params['search'])) {
                $qb->orwhere($qb->expr()->like('CONCAT( CONCAT(uc.ime, \' \'),  uc.prezime)', $qb->expr()->literal('%' . $params['search'] . '%')));
                $qb->orWhere($qb->expr()->like('CONCAT( CONCAT(raz.razred, \'.\'),  raz.odjel)', $qb->expr()->literal('%' . $params['search'] . '%')));
                $qb->orWhere($qb->expr()->like('po.naziv', $qb->expr()->literal('%' . $params['search'] . '%')));
            }

        $qb->andWhere('uc INSTANCE OF Application\Entity\Ucenik');


         $dql = $qb->getQuery()->getDQL();

        
        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }


    public function hasOrphans($id){
        $posudbe = $this->getAktivnaPosudba($id);

        return ($posudbe['aktivnePosudbe']>0) ? true : false;
    }

    public function recordExists($entity){
        return $recordExists = $this->getEntityManager()->getRepository('Application\Entity\Ucenik')->findOneBy(
            array(
                'ime' => $entity->getIme(),
                'prezime' => $entity->getPrezime(),
                'razred' => $entity->getRazred()->getId(),
            ));
    }


    public function getAktivnaPosudba($clanId){
        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Clan', 'uc')
            ->select('COUNT(p.id) as aktivnePosudbe ')
            ->leftJoin('uc.posudbe', 'p', 'WITH', 'p.updatedAt IS NULL')
            ->groupBy('uc.id')
            ->where('uc.id = :clanId')
            ->setParameter(':clanId', $clanId);


        return $qb->getQuery()->getOneOrNullResult();
    }
}