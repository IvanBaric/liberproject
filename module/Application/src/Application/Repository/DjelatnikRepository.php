<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;


// http://kristopherwilson.com/2013/02/26/using-doctrine-paginator-with-zend-framework-2-paginator/
class DjelatnikRepository extends EntityRepository {


    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Djelatnik', 'uc')
            ->select('uc.id as id, CONCAT( CONCAT(uc.ime, \' \'),  uc.prezime) as ime, uc.prezime as prezime,  COUNT(p.id) as aktivnePosudbe ')
            ->leftJoin('uc.posudbe', 'p', 'WITH', 'p.updatedAt IS NULL')
            ->groupBy('uc.id')
            ->orderBy($params['order_by'], $params['order']);

            if(!empty($params['search'])) {
                $qb->orwhere($qb->expr()->like('CONCAT( CONCAT(uc.ime, \' \'),  uc.prezime)', $qb->expr()->literal('%' . $params['search'] . '%')));
            }


        $qb->andWhere('uc INSTANCE OF Application\Entity\Djelatnik');



        $dql = $qb->getQuery()->getDQL();


        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }


    public function hasOrphans($id){
        $posudbe = $this->getAktivnaPosudba($id);

        return ($posudbe['aktivnePosudbe']>0) ? true : false;
    }


    public function getAktivnaPosudba($clanId){
        $qb = $this->_em->createQueryBuilder();
        $qb->from('Application\Entity\Clan', 'uc')
            ->select('COUNT(p.id) as aktivnePosudbe ')
            ->leftJoin('uc.posudbe', 'p', 'WITH', 'p.updatedAt IS NULL')
            ->groupBy('uc.id')
            ->where('uc.id = :clanId')
            ->setParameter(':clanId', $clanId);



        return $qb->getQuery()->getOneOrNullResult();
    }

}