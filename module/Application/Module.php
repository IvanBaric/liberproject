<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use User\Entity\User;
class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);


        $application = $e->getApplication();
        $em = $application->getEventManager();
        #$em->attach('route', array($this, 'onRoute'), -100);


        $serviceManager = $e->getTarget()->getServiceManager();
        $objectManager    = $serviceManager->get('Doctrine\ORM\EntityManager');

        $zfcAuthEvents    =  $serviceManager->get('ZfcUser\Authentication\Adapter\AdapterChain')->getEventManager();
        $zfcAuthEvents->attach('authenticate.success', function($authEvent) use ($serviceManager, $objectManager) {
            $userId = $authEvent->getIdentity();
            $user   = $objectManager->find(User::class, $userId);


            //
            $loginTable = new \User\Model\LoginTable($objectManager);
            $loginTable->logUser($user);


            if ($user->getRole()->getAbbrv() === 'member') {

                // we are going to re-set service,
                // we need to set allow_override of serviceManager= true
                $serviceManager->setAllowOverride(true);

                $zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
                $zfcuserModuleOptions->setLoginRedirectRoute('dashboard');
                $serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);

                // set 'allow_override' back to false
                $serviceManager->setAllowOverride(false);
            }




            if ($user->getRole()->getAbbrv() === 'admin') {

                // we are going to re-set service,
                // we need to set allow_override of serviceManager= true
                $serviceManager->setAllowOverride(true);

                $zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
                $zfcuserModuleOptions->setLoginRedirectRoute('dashboard');
                $serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);

                // set 'allow_override' back to false
                $serviceManager->setAllowOverride(false);
            }

            if ($user->getRole()->getAbbrv() === 'superadmin') {

                // we are going to re-set service,
                // we need to set allow_override of serviceManager= true
                $serviceManager->setAllowOverride(true);

                $zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
                $zfcuserModuleOptions->setLoginRedirectRoute('dashboard');
                $serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);

                // set 'allow_override' back to false
                $serviceManager->setAllowOverride(false);
            }
        });


    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig(){
        return array(
            'invokables' => array(
                'renderFormElement' => 'Application\View\Helper\RenderFormElement',
                'header' => 'Application\View\Helper\Header',
                'deleteDjeloBtn' => 'Application\View\Helper\DeleteDjeloBtn',
            ));
    }
}
