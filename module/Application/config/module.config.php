<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return array(
	 // Doctrine config
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                )
            ),
			'migrations_configuration' => array(
				'orm_default' => array(
					'directory' => 'data/migrations',
					'namespace' => 'Application\Migrations',
					'table' => 'migrations',
				),  
			),
            'zfcuser_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => __DIR__ . '/../src/Application/Entity',
            ),

            'configuration' => array(
                'orm_default' => array(
                    'repository_factory' => 'Application\ORM\Repository\DefaultRepositoryFactory'
                )
            )
        )
    ),


	
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),






            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),

            'zfcuser' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/dashboard',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Dashboard',
                        'action'     => 'index',
                    ),
                ),
            ),




            'dashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/dashboard',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Dashboard',
                        'action'     => 'index',
                    ),
                ),
            ),


            /***
             * UČENICI
             */

            'ucenici' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/ucenici',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Ucenik',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Ucenik',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Ucenik',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Ucenik',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),









                    'razredi' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/razredi',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Razred',
                                'action'        => 'index2',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(

                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Razred',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Razred',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Razred',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),
                        ),
                    ),































                ),
            ),


            /***
             * Djelatnici
             */

            'djelatnici' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/djelatnici',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Djelatnik',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelatnik',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelatnik',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelatnik',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),
                ),
            ),



            /***
             * RAZREDI
             */

//
//            'razredi' => array(
//                'type'    => 'Literal',
//                'options' => array(
//                    'route'    => '/razredi',
//                    'defaults' => array(
//                        '__NAMESPACE__' => 'Application\Controller',
//                        'controller'    => 'Razred',
//                        'action'        => 'index',
//                    ),
//                ),
//                'may_terminate' => true,
//                'child_routes' => array(
//
//                    'default' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '[/:action][/:id]',
//                            'constraints' => array(
//                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
//                                'id'     => '[a-zA-Z0-9_-]+',
//                            ),
//                            'defaults' => array(
//                            ),
//                        ),
//                    ),
//
//
//                    'edit' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '/edit[/:id]',
//                            'constraints' => array(
//                                'id'     => '[a-zA-Z0-9_-]+',
//                            ),
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Application\Controller',
//                                'controller'    => 'Razred',
//                                'action'        => 'edit',
//                            ),
//                        ),
//                    ),
//
//                    'delete' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '/delete[/:id]',
//                            'constraints' => array(
//                                'id'     => '[a-zA-Z0-9_-]+',
//                            ),
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Application\Controller',
//                                'controller'    => 'Razred',
//                                'action'        => 'delete',
//                            ),
//                        ),
//                    ),
//
//                    'ajaxSearch' => array(
//                        'type'    => 'Literal',
//                        'options' => array(
//                            'route'    => '/ajaxSearch',
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Application\Controller',
//                                'controller'    => 'Razred',
//                                'action'        => 'ajaxSearch',
//                            ),
//                        ),
//                    ),
//                ),
//            ),


            /***
             * PODRUČNI ODJEL
             */
            'podrucni-odjeli' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/podrucni-odjeli',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'PodrucniOdjel',
                        'action'        => 'index2',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'PodrucniOdjel',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'PodrucniOdjel',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'PodrucniOdjel',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),
                ),
            ),



            /***
             * Djelo
             */
            'liber-katalog' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/liber-katalog',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Djelo',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelo',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'confirmDelete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/confirm-delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelo',
                                'action'        => 'confirmDelete',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id][/:token]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                                'token'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelo',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelo',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),

                    'uspjesanUnos' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/uspjesan-unos',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Djelo',
                                'action'        => 'uspjesanUnos',
                            ),
                        ),
                    ),






                    /***
                     * IZDAVAC
                     */
                    'izdavaci' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/izdavaci',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Izdavac',
                                'action'        => 'index2',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(

                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Izdavac',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Izdavac',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Izdavac',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),
                        ),
                    ),


                    /***
                     * AUTOR
                     */
                    'autori' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/autori',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Autor',
                                'action'        => 'index2',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(

                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Autor',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Autor',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Autor',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),
                        ),
                    ),


                    /***
                     * KNJIZEVNA VRSTA
                     */
                    'knjizevne-vrste' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/knjizevne-vrste',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'KnjizevnaVrsta',
                                'action'        => 'index2',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(

                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'KnjizevnaVrsta',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'KnjizevnaVrsta',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'KnjizevnaVrsta',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),
                        ),
                    ),



                    /***
                     * KNJIZEVNA VRSTA
                     */
                    'mediji' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/mediji',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Medij',
                                'action'        => 'index2',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(

                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Medij',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Medij',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Application\Controller',
                                        'controller'    => 'Medij',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),
                        ),
                    ),










                ), // child routes
            ), // djelo




            /***
             * Knjižnični katalog
             */
            'knjiznicni-katalog' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/knjiznicni-katalog',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Katalog',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Katalog',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Katalog',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Katalog',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),





                    'novi-unos' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/novi-unos[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Katalog',
                                'action'        => 'noviunos',
                            ),
                        ),
                    ),



                    'detalji' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/id[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Katalog',
                                'action'        => 'detalji',
                            ),
                        ),
                    ),








                ),
            ),



            /***
             * Kataloški broj
             */
            'kataloski-broj' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/kataloski-broj',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'KataloskiBroj',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'KataloskiBroj',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'KataloskiBroj',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'KataloskiBroj',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),
                ),
            ),


            /***
             * Posudba
             */
            'posudba' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/posudba',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Posudba',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Posudba',
                                'action'        => 'edit',
                            ),
                        ),
                    ),

                    'delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/delete[/:id]',
                            'constraints' => array(
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Posudba',
                                'action'        => 'delete',
                            ),
                        ),
                    ),

                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Posudba',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),

                    'posudba' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/posudba',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Posudba',
                                'action'        => 'posudba',
                            ),
                        ),
                    ),
                ),
            ),





            /***
             * ALATI
             */

            'alati' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/knjiznicni-alati',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Alati',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '[/:action][/:id]',
                            'constraints' => array(
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


//                    'edit' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '/edit[/:id]',
//                            'constraints' => array(
//                                'id'     => '[0-9]+',
//                            ),
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Application\Controller',
//                                'controller'    => 'Ucenik',
//                                'action'        => 'edit',
//                            ),
//                        ),
//                    ),
//
//                    'delete' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '/delete[/:id]',
//                            'constraints' => array(
//                                'id'     => '[0-9]+',
//                            ),
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Application\Controller',
//                                'controller'    => 'Ucenik',
//                                'action'        => 'delete',
//                            ),
//                        ),
//                    ),

                    'upis' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/upis-u-visi-razred',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Alat',
                                'action'        => 'upisUVisiRazred',
                            ),
                        ),
                    ),

                    'postavke' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/postavke-knjiznice',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller'    => 'Alat',
                                'action'        => 'postavkeKnjiznice',
                            ),
                        ),
                    ),
                ),
            ),















        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
            'Application\ORM\Repository\CustomRepositoryFactory' => 'Application\ORM\Repository\Factory\CustomRepositoryFactoryFactory'
        ),
    ),
    'translator' => array(
        'locale' => 'hr_HR',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => Controller\IndexController::class,
            'Application\Controller\Ucenik' => Controller\UcenikController::class,
            'Application\Controller\Djelatnik' => Controller\DjelatnikController::class,
            'Application\Controller\Clan' => Controller\ClanController::class,
            'Application\Controller\Razred' => Controller\RazredController::class,
            'Application\Controller\PodrucniOdjel' => Controller\PodrucniOdjelController::class,
            'Application\Controller\Izdavac' => Controller\IzdavacController::class,
            'Application\Controller\Autor' => Controller\AutorController::class,
            'Application\Controller\KnjizevnaVrsta' => Controller\KnjizevnaVrstaController::class,
            'Application\Controller\Medij' => Controller\MedijController::class,
            'Application\Controller\Djelo' => Controller\DjeloController::class,
            'Application\Controller\Katalog' => Controller\KatalogController::class,
            'Application\Controller\KataloskiBroj' => Controller\KataloskiBrojController::class,
            'Application\Controller\Posudba' => Controller\PosudbaController::class,
            'Application\Controller\Alat' => Controller\AlatController::class,
            'Application\Controller\Dashboard' => Controller\DashboardController::class,
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
           'error/404'               => __DIR__ . '/../view/error/index.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
