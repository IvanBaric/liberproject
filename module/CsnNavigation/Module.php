<?php

namespace CsnNavigation;

use User\Entity\Role;
use Zend\View\HelperPluginManager;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Resource\GenericResource;

class Module
{
	protected $sm; // 
	
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
	
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function init(\Zend\ModuleManager\ModuleManager $mm)
	{
		// var_dump($mm);
	}
	
	public function onBootstrap(\Zend\EventManager\EventInterface $e) // use it to attach event listeners
	{
		$application = $e->getApplication();
		$this->sm = $application->getServiceManager();
		// var_dump($this->sm);
		
	}
	
    public function getViewHelperConfig()
    {
//		$auth = $this->sm->get('Zend\Authentication\AuthenticationService');
//		$config = $this->sm->get('Config');
//		$acl = new Acl($config);
		
        return array(
            'factories' => array(
                // This will overwrite the native navigation helper
                'navigation' => function(HelperPluginManager $pm) {
					$sm = $pm->getServiceLocator();
					$config = $sm->get('Config');
					
					// Setup ACL:
					// We have our own class.
					// ToDo think for better place for it maybe in CsnBase. I have it in CsnAuthorize and here \CsnNavigation\Acl\Acl
					$acl = new \CsnNavigation\Acl\Acl($config);
					

 					$auth = $sm->get('zfcuser_auth_service');
					
  					$role = Role::DEFAULT_ROLE; // The default role is guest $acl

					// With Doctrine
					if ($auth->hasIdentity()) {
						$user = $auth->getIdentity();
						$role = $user->getRole()->getAbbrv();
					}

						
					// Get an instance of the proxy helper
					$navigation = $pm->get('Zend\View\Helper\Navigation');
						
					// Store ACL and role in the proxy helper:
					$navigation->setAcl($acl)->setRole($role); // 'member'
					
					// Return the new navigation helper instance
					return $navigation;


                }
            )
        );
    }
	
}