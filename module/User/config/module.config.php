<?php
namespace User;

return array(

    'controllers' => array(
        'invokables' => array(
            #'Blog\Controller\Index' => Controller\IndexController::class,
            #'User\Controller\User' => Controller\UserController::class,
            'User\Controller\User' => Controller\UserController::class,

        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
           # 'MyClassNameCreator' => 'MyModule\Controller\Plugin\MyClassNameCreator',
        )
    ),
	
	'zfcuser' => array(
        // telling ZfcUser to use our own class
        'user_entity_class'       => 'User\Entity\User',
        // telling ZfcUserDoctrineORM to skip the entities it defines
        'enable_default_entities' => false,
    ),

    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                )
            ),
        ),
        'zfcuser_entity' => array(
            'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
            'paths' => __DIR__ . '/../src/User/Entity',
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'router' => array(
        'routes' => array(


            'usermanagement' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/user-management',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller'    => 'User',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(


                    'ajaxSearch' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/ajaxSearch',
                            'defaults' => array(
                                '__NAMESPACE__' => 'User\Controller',
                                'controller'    => 'User',
                                'action'        => 'ajaxSearch',
                            ),
                        ),
                    ),
					
					'edit' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/edit[/:id]',
							'constraints' => array(
								'id'     => '[a-zA-Z0-9_-]+',
							),
							'defaults' => array(
								'__NAMESPACE__' => 'User\Controller',
								'controller'    => 'User',
								'action'        => 'edit',
							),
						),
					),

					'delete' => array(
						'type'    => 'Segment',
						'options' => array(
							'route'    => '/delete[/:id]',
							'constraints' => array(
								'id'     => '[a-zA-Z0-9_-]+',
							),
							'defaults' => array(
								'__NAMESPACE__' => 'User\Controller',
								'controller'    => 'User',
								'action'        => 'delete',
							),
						),
					),



             




            











                    // SETTINGS

                    'settings' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/settings',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Blog\Controller',
                                'controller'    => 'Settings',
                                'action'        => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/[:controller[/:action]]',
                                    'constraints' => array(
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),


                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Settings',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Settings',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),

                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Settings',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),


                        ),
                    ),





                ),
            ), // end blog



            'page' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/page',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Blog\Controller',
                        'controller'    => 'Frontend',
                        'action'        => 'page',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/:abbrv',
                            'constraints' => array(
                                'abbrv'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),


                    'static' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/static',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Blog\Controller',
                                'controller'    => 'Page',
                                'action'        => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/[:controller[/:action]]',
                                    'constraints' => array(
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),

                            'edit' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/edit[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Page',
                                        'action'        => 'edit',
                                    ),
                                ),
                            ),

                            'delete' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/delete[/:id]',
                                    'constraints' => array(
                                        'id'     => '[a-zA-Z0-9_-]+',
                                    ),
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Page',
                                        'action'        => 'delete',
                                    ),
                                ),
                            ),



                            'ajaxSearch' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/ajaxSearch',
                                    'defaults' => array(
                                        '__NAMESPACE__' => 'Blog\Controller',
                                        'controller'    => 'Page',
                                        'action'        => 'ajaxSearch',
                                    ),
                                ),
                            ),


                        ),
                    ),








                ),
            ), // end page










        )
    )

);