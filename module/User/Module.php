<?php

namespace User;
use User\Entity\User;



class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
	
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	// FOR Authorization
	public function onBootstrap(\Zend\EventManager\EventInterface $e) // use it to attach event listeners
	{
		$application = $e->getApplication();
		$em = $application->getEventManager();
//		$em->attach('route', array($this, 'onRoute'), -100);


//
//
//		$serviceManager = $e->getTarget()->getServiceManager();
//		$objectManager    = $serviceManager->get('Doctrine\ORM\EntityManager');
//
//		$zfcAuthEvents    =  $serviceManager->get('ZfcUser\Authentication\Adapter\AdapterChain')->getEventManager();
//		$zfcAuthEvents->attach('authenticate.success', function($authEvent) use ($serviceManager, $objectManager) {
//			$userId = $authEvent->getIdentity();
//			$user   = $objectManager->find(User::class, $userId);
//
//
//			//
//			$loginTable = new \User\Model\LoginTable($objectManager);
//			$loginTable->logUser($user);
//
//
//			if ($user->getRole()->getAbbrv() === 'member') {
//
//				// we are going to re-set service,
//				// we need to set allow_override of serviceManager= true
//				$serviceManager->setAllowOverride(true);
//
//				$zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
//				$zfcuserModuleOptions->setLoginRedirectRoute('shop');
//				$serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);
//
//				// set 'allow_override' back to false
//				$serviceManager->setAllowOverride(false);
//			}
//
//
//
//
//			if ($user->getRole()->getAbbrv() === 'admin') {
//
//				// we are going to re-set service,
//				// we need to set allow_override of serviceManager= true
//				$serviceManager->setAllowOverride(true);
//
//				$zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
//				$zfcuserModuleOptions->setLoginRedirectRoute('shop');
//				$serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);
//
//				// set 'allow_override' back to false
//				$serviceManager->setAllowOverride(false);
//			}
//
//			if ($user->getRole()->getAbbrv() === 'superadmin') {
//
//				// we are going to re-set service,
//				// we need to set allow_override of serviceManager= true
//				$serviceManager->setAllowOverride(true);
//
//				$zfcuserModuleOptions = $serviceManager->get('zfcuser_module_options');
//				$zfcuserModuleOptions->setLoginRedirectRoute('dashboard');
//				$serviceManager->setService('zfcuser_module_options', $zfcuserModuleOptions);
//
//				// set 'allow_override' back to false
//				$serviceManager->setAllowOverride(false);
//			}
//		});



	}


	public function getServiceConfig()
	{
		return array(
				'abstract_factories' => array(),
				'aliases' => array(),
				'factories' => array(),
				'invokables' => array(),
				'services' => array(),
				'shared' => array(),
		);
	}

	public function getViewHelperConfig(){
		return array(
				'invokables' => array(
						#'settings' => 'Blog\View\Helper\Settings',
				),
				'factories' => array()
		);
	}

}