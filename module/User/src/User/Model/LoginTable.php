<?php
namespace User\Model;

use User\Entity\Login;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use \MyModule\Model\MyModel;
class LoginTable extends MyModel
{
    protected $_em;

    public function __construct(ObjectManager $objectManager)
    {
        $this->_em = $objectManager;
    }


    public function logUser(\User\Entity\User $user){
        $login = new Login();
        $login->setUser($user);
        $login->setLoggedIn();
        $this->_em->persist($login);
        $this->_em->flush();
    }
}