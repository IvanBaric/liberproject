<?php
namespace User\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\User\Repository\RoleRepository")
 * @ORM\Table(name="user_role", options={"collate"="utf8_general_ci", "charset"="utf8"});
 * @ORM\HasLifecycleCallbacks()
 */
class Role {


    const DEFAULT_ROLE = 'guest';
    const MEMBER = 'member';
    const ADMIN  = 'admin';
    const SUPERADMIN  = 'superadmin';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $name;


    /**
     * @var string
     *
     * @ORM\Column(name="abbrv", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $abbrv;





    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAbbrv()
    {
        return $this->abbrv;
    }

    /**
     * @param string $abbrv
     */
    public function setAbbrv($abbrv)
    {
        $this->abbrv = $abbrv;
    }







}