<?php
namespace User\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use MyModule\Repository\MyRepository;

class RoleRepository extends MyRepository {


    public function getDropdown(){
        $roles = $this->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $data = array();
        foreach($roles as $role){
            $id = $role->getId();
            $name = $role->getName();


            $data[$id] = $name;
        }

        return $data;
    }



}