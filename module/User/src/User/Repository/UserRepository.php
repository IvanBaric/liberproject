<?php
namespace User\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use MyModule\Repository\MyRepository;

class UserRepository extends MyRepository {

    public function fetchAll($params){
        $qb = $this->_em->createQueryBuilder();

        $qb->from('User\Entity\User', 'u')
            ->select('u, r, a')
            ->join('u.role', 'r')
            ->leftJoin('u.address', 'a')
            ->orderBy($params['order_by'], $params['order']);

        if(!empty($params['search'])) {
            $qb->orWhere($qb->expr()->like('u.email', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('u.username', $qb->expr()->literal('%' . $params['search'] . '%')));
            $qb->orWhere($qb->expr()->like('r.name', $qb->expr()->literal('%' . $params['search'] . '%')));
        }

        $dql = $qb->getQuery()->getDQL();


        $query = $this->_em->createQuery($dql)->setMaxResults($params['ipp'])->setFirstResult($params['offset']);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        return $paginator;
    }

}