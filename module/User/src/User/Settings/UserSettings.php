<?php
namespace User\Settings;
use \MyModule\Settings\MySettings;

class UserSettings extends MySettings {

    public function __construct(){
        parent::__construct();
    }

    #public $hasOrphansClass = "Blog\Entity\Post";
    #public $hasOrphansProperty = "category";

    public $defaultTableOrderByColumn = 'u.id';

    public $entityRoute = "usermanagement";



}