<?php
namespace User\Form;

use Zend\Form\Form;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\Factory as InputFactory;

use User\Entity\User;


class UserForm extends Form {

	
	public function __construct(ObjectManager $objectManager)
	{



		parent::__construct('UserForm');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		#$this->setHydrator(new DoctrineHydrator($objectManager));

		$this->setHydrator(new DoctrineHydrator($objectManager, true))->setObject(new User());

		$roles = $objectManager->getRepository('User\Entity\Role')->getDropdown();


//		$this->add(array(
//				'name' => 'username',
//				'options' => array(
//						'label' => 'Username',
//				),
//				'attributes' => array(
//						'type' => 'text'
//				),
//		));

		$this->add(array(
				'name' => 'email',
				'options' => array(
						'label' => 'Email',
				),
				'attributes' => array(
						'type' => 'text'
				),
		));

		$this->add(array(
				'name' => 'displayName',
				'options' => array(
						'label' => 'Display Name',
				),
				'attributes' => array(
						'type' => 'text'
				),
		));

//		$this->add(array(
//				'name' => 'password',
//				'type' => 'password',
//				'options' => array(
//						'label' => 'Password',
//				),
//				'attributes' => array(
//						'type' => 'password'
//				),
//		));
//
//		$this->add(array(
//				'name' => 'passwordVerify',
//				'type' => 'password',
//				'options' => array(
//						'label' => 'Password Verify',
//				),
//				'attributes' => array(
//						'type' => 'password'
//				),
//		));


		$this->add(array(
				'type' => 'Zend\Form\Element\Select',
				'name' => 'role',
				'attributes' => array(
						'class' => '',
				),
				'options' => array(
						'label' => 'Role',
						'empty_option' => 'Odaberite',
						'value_options' => $roles
				)
		));


		$this->add(new \Zend\Form\Element\Csrf('csrf'));

		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'submit',
				),
		));

	}






	public function getInputFilter()
	{
		if (! $this->filter) {
			$inputFilter = new InputFilter ();
			$factory = new InputFactory ();
//			$inputFilter->add(array(
//					'name'       => 'username',
//					'required'   => true,
//					'validators' => array(
//							array(
//									'name'    => 'StringLength',
//									'options' => array(
//											'min' => 3,
//											'max' => 255,
//									),
//							),
//							#$this->usernameValidator,
//					),
//			));

			$inputFilter->add(array(
					'name'       => 'email',
					'required'   => true,
					'validators' => array(
							array(
									'name' => 'EmailAddress'
							),
							#$this->emailValidator
					),
			));

			$inputFilter->add(array(
					'name'       => 'displayName',
					'required'   => true,
					'filters'    => array(array('name' => 'StringTrim')),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'min' => 3,
											'max' => 128,
									),
							),
					),
			));

//			$inputFilter->add(array(
//					'name'       => 'password',
//					'required'   => true,
//					'filters'    => array(array('name' => 'StringTrim')),
//					'validators' => array(
//							array(
//									'name'    => 'StringLength',
//									'options' => array(
//											'min' => 6,
//									),
//							),
//					),
//			));
//
//			$inputFilter->add(array(
//					'name'       => 'passwordVerify',
//					'required'   => true,
//					'filters'    => array(array('name' => 'StringTrim')),
//					'validators' => array(
//							array(
//									'name'    => 'StringLength',
//									'options' => array(
//											'min' => 6,
//									),
//							),
//							array(
//									'name'    => 'Identical',
//									'options' => array(
//											'token' => 'password',
//									),
//							),
//					),
//			));

			$inputFilter->add ( $factory->createInput(
					array (
							'name' => 'role',
							'filters' => array (array ('name' => 'StripTags'),array ('name' => 'StringTrim')),
							'validators' => array()
					)));



			$this->filter = $inputFilter;

		}

		return $this->filter;

	}




}