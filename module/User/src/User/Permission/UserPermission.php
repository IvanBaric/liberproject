<?php
namespace User\Permission;

use MyModule\Permission\MyPermission;
use User\Entity\Role;

class UserPermission extends MyPermission {


    public function __construct($user){
        parent::__construct($user);

        $this->deny(Role::ADMIN, array(MyPermission::DELETE, MyPermission::UPDATE, MyPermission::CREATE));
        #$this->allow(Role::ADMIN, array(MyPermission::DELETE_USER_ENTITY));
    }

}