<?php
// http://p0l0.binware.org/index.php/2012/02/18/zend-framework-2-authentication-acl-using-eventmanager/
// First I created an extra config for ACL (could be also in module.config.php, but I prefer to have it in a separated file)
return array(
    'acl' => array(
        'roles' => array(
            'guest'   => null,
            'member'  => 'guest',
            'admin'  => 'member',
			'superadmin' => 'admin'
        ),
        'resources' => array(
            'allow' => array(
                'user' => array(
                    'login' => 'guest',
                    'all'   => 'member'
                ),

            	'zfcuser'	=>	array(
            		'index'		=> 'guest',
            		'login'		=> 'guest',
            		'logout'	=> 'guest',
            		'register'	=> 'guest'
            	),

				'MyModule\Controller\Error'	=>	array(
						'all' => 'member'
				),

					'Application\Controller\Index'	=>	array(
							'all' => 'guest'
					),
            		


					'Application\Controller\Index'	=>	array(
							'index'	=> 'guest'
					),

					'Application\Controller\Dashboard'	=>	array(
							'all' => 'member'
					),

					'Application\Controller\Ucenik'	=>	array(
							'all' => 'member'

					),

					'Application\Controller\Djelatnik'	=>	array(
							'all' => 'admin'

					),

					'Application\Controller\Razred'	=>	array(
							'all' => 'admin'

					),

					'Application\Controller\PodrucniOdjel'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Izdavac'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Autor'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\KnjizevnaVrsta'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Medij'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Djelo'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Katalog'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\KataloskiBroj'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Posudba'	=>	array(
							'all' => 'admin'
					),

					'Application\Controller\Alat'	=>	array(
							'all' => 'admin'
					),




					// USER

					'User\Controller\User'	=>	array(
							'index' => 'admin',
							'edit' => 'superadmin',
							'delete' => 'superadmin',
							'ajaxSearch' => 'admin'
					),
            )
        )
    )
);
