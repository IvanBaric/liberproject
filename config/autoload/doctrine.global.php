<?php
return array(
  'doctrine' => array(
		'connection' => array(
			'orm_default' => array(
				'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
				'doctrine_type_mappings' => array(
					'enum' => 'string'
				),
				'params' => array(
				  'host'     => 'localhost',
				  'port'     => '3306',
				  'user'     => 'root',
				  'password' => '',
				  'dbname'   => 'liber',
				  'charset'	=> 'UTF8',
				  'collate'	=> 'utf8_general_ci'
				)
			)
		),
		'configuration' => array(
            'orm_default' => array(
                // Generate proxies should be FALSE on production servers
                'generate_proxies' => true,
            ),
        ),
	)
);