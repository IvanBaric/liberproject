<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
// from http://framework.zend.com/manual/2.1/en/modules/zend.navigation.quick-start.html
// the array was empty before that
return array( // ToDO make it dynamic - comes from the DB
     'navigation' => array(
				'default' => array(

// S H O P
						array(
								'label' => 'Shop',
								'route' => 'shop',
								'resource'	=> 'Shop\Controller\Category',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-shopping-cart',
								'pages' => array(
										array(
												'label' => 'Proizvodi',
												'route' => 'shop/product',
												'resource'	=> 'Shop\Controller\Product',
												'privilege'	=> 'edit',
												'data-icon' => 'fa fa-folder-open',
										),

										array(
												'label' => 'Novi proizvod',
												'route' => 'shop/product/edit',
												'resource'	=> 'Shop\Controller\Product',
												'privilege'	=> 'edit',
												'data-icon' => 'fa fa-folder-open',
										),

										array(
												'label' => 'Kategorije',
												'route' => 'shop/categories',
												'resource'	=> 'Shop\Controller\Category',
												'privilege'	=> 'edit',
												'data-icon' => 'fa fa-folder-open',
										),


								)
						),



//Blog
						array(
								'label' => 'Objave',
								'route' => 'blog/posts',
								'resource'	=> 'Blog\Controller\Post',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-book',
								'pages' => array(
										array(
												'label' => 'Sve objave',
												'route' => 'blog/posts',
												'resource'	=> 'Blog\Controller\Post',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Nova objava',
												'route' => 'blog/posts/edit',
												'resource'	=> 'Blog\Controller\Post',
												'privilege'	=> 'edit',
										),

										array(
												'label' => 'Oznake',
												'route' => 'blog/tags',
												'resource'	=> 'Blog\Controller\Tag',
												'privilege'	=> 'edit',
										),

										array(
												'label' => 'Kategorije',
												'route' => 'blog/categories',
												'resource'	=> 'Blog\Controller\Category',
												'privilege'	=> 'edit',
										),

										array(
												'label' => 'Postavke objava',
												'route' => 'blog/settings',
												'resource'	=> 'Blog\Controller\Settings',
												'privilege'	=> 'edit',
										),


								),
						),

						array(
								'label' => 'Stranice',
								'route' => 'page/static',
								'resource'	=> 'Blog\Controller\Page',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-user',
								'pages' => array(
										array(
												'label' => 'Sve stranice',
												'route' => 'page/static',
												'resource'	=> 'Blog\Controller\Page',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Nova stranica',
												'route' => 'page/static/edit',
												'resource'	=> 'Blog\Controller\Page',
												'privilege'	=> 'edit',
										),
								)
						),



						//Liber
						array(
								'label' => 'Kontrolna ploča',
								'route' => 'dashboard',
								'resource'	=> 'Application\Controller\Dashboard',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-user',
						),


						array(
								'label' => 'Učenici',
								'route' => 'ucenici',
								'resource'	=> 'Application\Controller\Ucenik',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-users',
								'pages' => array(
									array(
											'label' => 'Popis učenika',
											'route' => 'ucenici',
											'resource'	=> 'Application\Controller\Ucenik',
											'privilege'	=> 'edit',
									),

										array(
												'label' => 'Novi učenik',
												'route' => 'ucenici/edit',
												'resource'	=> 'Application\Controller\Ucenik',
												'privilege'	=> 'edit',
										),

									array(
											'label' => 'Razredi',
											'route' => 'ucenici/razredi',
											'resource'	=> 'Application\Controller\Razred',
											'privilege'	=> 'index',
											'pages' => array(
													array(
															'label' => 'Popis razreda',
															'route' => 'ucenici/razredi',
															'resource'	=> 'Application\Controller\Razred',
															'privilege'	=> 'index',
													),

													array(
															'label' => 'Novi razred',
															'route' => 'ucenici/razredi/edit',
															'resource'	=> 'Application\Controller\Razred',
															'privilege'	=> 'edit',
													),


											),









									),

//									array(
//											'label' => 'Razredi',
//											'route' => 'razredi',
//											'resource'	=> 'Application\Controller\Razred',
//											'privilege'	=> 'index',
//									),
//										array(
//												'label' => 'Novi razred',
//												'route' => 'razredi/edit',
//												'resource'	=> 'Application\Controller\Razred',
//												'privilege'	=> 'edit',
//										),


								),
						),

						array(
								'label' => 'Djelatnici',
								'route' => 'djelatnici',
								'resource'	=> 'Application\Controller\Djelatnik',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-user',
								'pages' => array(
										array(
												'label' => 'Popis djelatnika',
												'route' => 'djelatnici',
												'resource'	=> 'Application\Controller\Djelatnik',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Novi djelatnik',
												'route' => 'djelatnici/edit',
												'resource'	=> 'Application\Controller\Djelatnik',
												'privilege'	=> 'edit',






										),


								),
						),


//						array(
//								'label' => 'Razredi',
//								'route' => 'razredi',
//								'resource'	=> 'Application\Controller\Razred',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-users',
//								'pages' => array(
//										array(
//												'label' => 'Popis razreda',
//												'route' => 'razredi',
//												'resource'	=> 'Application\Controller\Razred',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Novi razred',
//												'route' => 'razredi/edit',
//												'resource'	=> 'Application\Controller\Razred',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),


//						array(
//								'label' => 'Područni odjeli',
//								'route' => 'podrucni-odjeli',
//								'resource'	=> 'Application\Controller\PodrucniOdjel',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-building',
//								'pages' => array(
//										array(
//												'label' => 'Popis područnih odjela',
//												'route' => 'podrucni-odjeli',
//												'resource'	=> 'Application\Controller\PodrucniOdjel',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Novi područni odjel',
//												'route' => 'podrucni-odjeli/edit',
//												'resource'	=> 'Application\Controller\PodrucniOdjel',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),


//						array(
//								'label' => 'Izdavači',
//								'route' => 'izdavaci',
//								'resource'	=> 'Application\Controller\Izdavac',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-building',
//								'pages' => array(
//										array(
//												'label' => 'Popis izdavača',
//												'route' => 'izdavaci',
//												'resource'	=> 'Application\Controller\Izdavac',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Novi izdavač',
//												'route' => 'izdavaci/edit',
//												'resource'	=> 'Application\Controller\Izdavac',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),

//						array(
//								'label' => 'Autori',
//								'route' => 'autori',
//								'resource'	=> 'Application\Controller\Autor',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-building',
//								'pages' => array(
//										array(
//												'label' => 'Popis autora',
//												'route' => 'autori',
//												'resource'	=> 'Application\Controller\Autor',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Novi autor',
//												'route' => 'autori/edit',
//												'resource'	=> 'Application\Controller\Autor',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),

//						array(
//								'label' => 'Književne vrste',
//								'route' => 'knjizevne-vrste',
//								'resource'	=> 'Application\Controller\KnjizevnaVrsta',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-book',
//								'pages' => array(
//										array(
//												'label' => 'Popis književnih vrsta',
//												'route' => 'knjizevne-vrste',
//												'resource'	=> 'Application\Controller\KnjizevnaVrsta',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Nova književna vrsta',
//												'route' => 'knjizevne-vrste/edit',
//												'resource'	=> 'Application\Controller\KnjizevnaVrsta',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),


//						array(
//								'label' => 'Mediji',
//								'route' => 'mediji',
//								'resource'	=> 'Application\Controller\Medij',
//								'privilege'	=> 'index',
//								'data-icon' => 'fa fa-book',
//								'pages' => array(
//										array(
//												'label' => 'Popis medija',
//												'route' => 'mediji',
//												'resource'	=> 'Application\Controller\Medij',
//												'privilege'	=> 'index',
//										),
//
//										array(
//												'label' => 'Novi medij',
//												'route' => 'mediji/edit',
//												'resource'	=> 'Application\Controller\Medij',
//												'privilege'	=> 'edit',
//										),
//
//
//								),
//						),

						array(
								'label' => 'Liber katalog',
								'route' => 'liber-katalog',
								'resource'	=> 'Application\Controller\Djelo',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-book',
								'pages' => array(
										array(
												'label' => 'Popis knjiženo-umjetničkih djela',
												'route' => 'liber-katalog',
												'resource'	=> 'Application\Controller\Djelo',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Mediji',
												'route' => 'liber-katalog/mediji',
												'resource'	=> 'Application\Controller\Medij',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Izdavači',
												'route' => 'liber-katalog/izdavaci',
												'resource'	=> 'Application\Controller\Izdavac',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Književne vrste',
												'route' => 'liber-katalog/knjizevne-vrste',
												'resource'	=> 'Application\Controller\KnjizevnaVrsta',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Autori',
												'route' => 'liber-katalog/autori',
												'resource'	=> 'Application\Controller\Autor',
												'privilege'	=> 'index',
										),

//										array(
//												'label' => 'Novo književno-umjetničko djelo',
//												'route' => 'katalog/edit',
//												'resource'	=> 'Application\Controller\Djelo',
//												'privilege'	=> 'edit',
//										),



								),
						),


						array(
								'label' => 'Moj katalog',
								'route' => 'knjiznicni-katalog',
								'resource'	=> 'Application\Controller\Katalog',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-book',
								'pages' => array(
//										array(
//												'label' => 'Moj katalog',
//												'route' => 'knjiznicni-katalog',
//												'resource'	=> 'Application\Controller\Katalog',
//												'privilege'	=> 'index',
//										),

//										array(
//												'label' => 'Zajednički katalog',
//												'route' => 'katalog',
//												'resource'	=> 'Application\Controller\Djelo',
//												'privilege'	=> 'index',
//										),

//										array(
//												'label' => 'Novo književno-umjetničko djelo',
//												'route' => 'katalog/edit',
//												'resource'	=> 'Application\Controller\Djelo',
//												'privilege'	=> 'edit',
//										),
//
//										array(
//												'label' => 'Uredi književno djelo',
//												'route' => 'knjiznicni-katalog/edit',
//												'resource'	=> 'Application\Controller\Katalog',
//												'privilege'	=> 'edit',
//										),


								),
						),


						array(
								'label' => 'Posudbe',
								'route' => 'posudba',
								'resource'	=> 'Application\Controller\Posudba',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-book',
								'pages' => array(
										array(
												'label' => 'Prikaz svih posudbi',
												'route' => 'posudba',
												'resource'	=> 'Application\Controller\Posudba',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Nova posudba',
												'route' => 'posudba/posudba',
												'resource'	=> 'Application\Controller\Posudba',
												'privilege'	=> 'index',
										),


								),
						),

						array(
								'label' => 'Alati',
								'route' => 'alati',
								'resource'	=> 'Application\Controller\Alat',
								'privilege'	=> 'index',
								'data-icon' => 'fa fa-tasks',
								'pages' => array(

										array(
												'label' => 'Upravljanje područnim odjelima',
												'route' => 'podrucni-odjeli',
												'resource'	=> 'Application\Controller\PodrucniOdjel',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Upis učenika u viši razred',
												'route' => 'alati/upis',
												'resource'	=> 'Application\Controller\Alat',
												'privilege'	=> 'index',
										),

										array(
												'label' => 'Postavke knjižnice',
												'route' => 'alati/postavke',
												'resource'	=> 'Application\Controller\Alat',
												'privilege'	=> 'edit',
										),


								),
						),





				),		  
     ),
     'service_manager' => array(
         'factories' => array(
             'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
			 // 'secondary_navigation' => 'CsnNavigation\Navigation\Service\SecondaryNavigationFactory',
			 #'secondary_navigation' => 'Csn\Zend\Navigation\Service\SecondaryNavigationFactory',
         ),
     ),
);

/*
action	String	NULL	Action name to use when generating href to the page.
controller	String	NULL	Controller name to use when generating href to the page.
params	Array	array()	User params to use when generating href to the page.
route	String	NULL	Route name to use when generating href to the page.
routeMatch	Zend\Mvc\Router\RouteMatch	NULL	RouteInterface matches used for routing parameters and testing validity.
router	Zend\Mvc\Router\RouteStackInterface	NULL	Router for assembling URLs
*/